<?php
namespace app\admin\loglic;

class Upload
{
    public function fields()
    {
        $fields = [
            'upload_path' => [
                'type'        => 'text',
                'value'       => config('common.upload_path'),
                'required'    => true,
                'placeholder' => lang('upload_path_placeholder'),
            ],
            'upload_save_rule'  => [
                'type'        => 'text',
                'value'       => DcEmpty(config('common.upload_save_rule'),'date'),
                'placeholder' => lang('upload_save_rule_placeholder'),
            ],
            'upload_max_size'  => [
                'type'        => 'text',
                'value'       => config('common.upload_max_size'),
                'placeholder' => lang('upload_max_size_placeholder'),
            ],
            'upload_file_ext'  => [
                'type'        => 'text',
                'value'       => config('common.upload_file_ext'),
                'placeholder' => lang('upload_file_ext_placeholder'),
            ],
            'upload_mime_type' => [
                'type'        => 'text',
                'value'       => config('common.upload_mime_type'),
                'placeholder' => lang('upload_mime_type_placeholder'),
            ],
            'upload_referer' => [
                'type'        => 'text',
                'value'       => config('common.upload_referer'),
                'placeholder' => lang('upload_referer_placeholder'),
            ],
            'upload_host' => [
                'type'        => 'text',
                'value'       => config('common.upload_host'),
                'placeholder' => lang('upload_host_placeholder'),
            ],
            'upload_cdn' => [
                'type'        => 'text',
                'value'       => config('common.upload_cdn'),
                'placeholder' => lang('upload_cdn_placeholder'),
            ],
            'hr' => [
                'type'        => 'html',
                'value'       => '<hr>',
            ],
            'upload_thumb_type' => [
                'type'        => 'text',
                'value'       => config('common.upload_thumb_type'),
                'placeholder' => lang('upload_thumb_type_placeholder'),
            ],
            'upload_thumb_width' => [
                'type'        => 'text',
                'value'       => config('common.upload_thumb_width'),
                'placeholder' => lang('upload_thumb_width_placeholder'),
            ],
            'upload_thumb_height' => [
                'type'        => 'text',
                'value'       => config('common.upload_thumb_height'),
                'placeholder' => lang('upload_thumb_height_placeholder'),
            ],
            'hr2' => [
                'type'        => 'html',
                'value'       => '<hr>',
            ],
            'upload_water_locate' => [
                'type'        => 'text',
                'value'       => intval(config('common.upload_water_locate')),
                'placeholder' => lang('upload_water_locate_placeholder'),
            ],
            'upload_water_logo' => [
                'type'        => 'text',
                'value'       => config('common.upload_water_logo'),
                'placeholder' => lang('upload_water_logo_placeholder'),
            ],
            'upload_water_light' => [
                'type'        => 'text',
                'value'       => intval(config('common.upload_water_light')),
            ],
            'hr3' => [
                'type'        => 'html',
                'value'       => '<hr>',
            ],
            'upload_water_text' => [
                'type'        => 'text',
                'value'       => config('common.upload_water_text'),
                'placeholder' => lang('upload_water_text_placeholder'),
            ],
            'upload_water_font' => [
                'type'        => 'text',
                'value'       => config('common.upload_water_font'),
                'placeholder' => lang('upload_water_font_placeholder'),
            ],
            'upload_water_size' => [
                'type'        => 'text',
                'value'       => config('common.upload_water_size'),
                'placeholder' => lang('upload_water_size_placeholder'),
            ],
            'upload_water_color' => [
                'type'        => 'text',
                'value'       => config('common.upload_water_color'),
                'placeholder' => lang('upload_water_color_placeholder'),
            ],
            'upload_water_offset' => [
                'type'        => 'text',
                'value'       => intval(config('common.upload_water_offset')),
                'placeholder' => lang('upload_water_offset_placeholder'),
            ],
            'upload_water_angle' => [
                'type'        => 'text',
                'value'       => intval(config('common.upload_water_angle')),
                'placeholder' => lang('upload_water_angle_placeholder'),
            ],
        ];
        //合并动态扩展字段
        if($customs = model('common/Config','loglic')->metaList('admin', 'upload')){
            $fields = array_merge($fields,DcFields($customs, config('common')));
        }
        return $fields;
    }
}