{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords}" />
<meta name="description" content="{$seoDescription}" />
{/block}
<!-- -->
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
{include file="widget/ads970" /}
{foreach name=":daohangCategorySelect([
'cache'  => true,
'status' => 'normal',
'result' => 'tree',
'sort'   => 'term_order',
'order'  => 'desc',
])" item="types"}
<div class="card mb-3">
  <div class="card-header">
    <a class="font-weight-bold" href="{:daohangUrlCategory($types)}" target="_self">{$types.term_name}</a>
  </div>
  <div class="card-body row">
    {foreach name="$types['_child']" id="category"}
    <div class="col-4 col-md-2 py-3">
      <a class="btn btn-light btn-block btn-sm py-3" href="{:daohangUrlCategory($category)}" target="_self">{$category.term_name}</a>
    </div>
    {/foreach}
  </div>
</div>
{/foreach}
</div>
{/block}
{block name="footer"}{include file="widget/footer" /}{/block}