<?php
namespace app\admin\controller;

use think\Controller;

class Mysql extends Controller
{
    //安装界面
	public function index()
    {
        $this->isInstall();
        
        return $this->fetch();
	}
    
    //初始数据
    public function import()
    {
        $this->isInstall();
        
        //框架初始数据
        model('admin/Datas','loglic')->install();
        
        //增加锁文件
        touch('./datas/db/mysql.lock');
        
        //安装应用(回调完整应用的一键安装脚本)
        model(INSTALL_NAME.'/Install','loglic')->mysql();

        //返回结果
        $this->success(lang('mysql_install_success'), '/admin.php/index/login');
    }
    
    //安装数据库
    public function save()
    {
        //安装权限
        $this->isInstall();
        
        //写入权限验证
        if(!is_writable('./apps/database.php')){
            $this->error(lang('mysql_error_write')); 
        }
        
        //验证函数
        if(!function_exists(mysqli_connect)){
            $this->error(lang('mysql_error_exit'));
        }
        
        //MYSQL配置
        $data = [
            'type'            => 'mysql',
            'charset'         => 'utf8',
            'hostname'        => input('post.mysql_hostname/s', '127.0.0.1'),
            'database'        => input('post.mysql_database/s', 'daicuo'),
            'username'        => input('post.mysql_username/s', 'root'),
            'password'        => input('post.mysql_password/s', 'root'),
            'hostport'        => input('post.mysql_port/d', '3306'),
            'prefix'          => input('post.mysql_prefix/s', 'dc_'),
            'resultset_type'  => 'collection',
            'datetime_format' => 'Y-m-d H:i:s',
        ];

        //验证MYSQL连接参数
        $connect = @mysqli_connect($data['hostname'].":".intval($data['hostport']), $data['username'], $data['password']);
        if($connect == false){
            $this->error(lang('mysql_error_info'));
        }
        //数据库不存在,尝试建立
        if( !@mysqli_select_db($data['database']) ){
             //创建数据库语句
			 mysqli_query($connect, "CREATE DATABASE `".$data["database"]."` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci");
            //数据库创建失败
            if( !@mysqli_select_db($connect, $data['database']) ){
                $this->error(lang('mysql_user_low'));
            }
        }
        
        //执行SQL
        $this->exceSql($connect);
        
        //手动关闭MYSQL链接
        mysqli_close($connect);
        
        //修改MYSQL连接配置
        write_array('./apps/database.php', $data);
        
        //直接跳转导入数据
        $this->redirect('mysql/import');
        
        //$this->success('ok','mysql/import');
    }
    
    //执行SQL语句
    private function exceSql($connect='')
    {
        $sqls = file_get_contents('./datas/default/install.sql');
        
        $sqlArray = explode(';n', str_replace(array(";\r\n", ";\r", ";\n"), ";n", $sqls));
        
        foreach($sqlArray as $sql){
            if($sql){
                mysqli_query($connect, $sql);
            }
        }
    }
    
    //验证是否已安装
    private function isInstall()
    {
        if(!defined('INSTALL_NAME')){
            $this->error(lang('mustIn'));
        }
        
        if( is_file('./datas/db/mysql.lock') ){
			$this->error(lang('mysql_install_ready'));
		}
    }
}