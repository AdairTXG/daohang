<?php
namespace app\daohang\loglic;

class Count
{
    public function views()
    {
        return db('info')->where(['info_module'=>'daohang'])->sum('info_views');
    }
    
    public function detail()
    {
        return db('info')->where(['info_module'=>'daohang','info_status'=>'normal'])->count('info_id');
    }
    
    public function hidden()
    {
        return db('info')->where(['info_module'=>'daohang','info_status'=>'hidden'])->count('info_id');
    }
    
    public function category()
    {
        return db('term')->where(['term_module'=>'daohang','term_controll'=>'category'])->count('term_id');
    }
    
    public function tag()
    {
        return db('term')->where(['term_module'=>'daohang','term_controll'=>'tag'])->count('term_id');
    }
}