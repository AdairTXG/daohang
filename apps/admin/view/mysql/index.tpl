<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="renderer" content="webkit">
<title>{:lang('admin/mysql/index')}</title>
<link rel="stylesheet" href="https://lib.baomitu.com/twitter-bootstrap/4.6.1/css/bootstrap.min.css">
<script src="https://lib.baomitu.com/jquery/3.3.1/jquery.min.js"></script>
<script src="https://lib.baomitu.com/twitter-bootstrap/4.6.1/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<div class="container pt-5">
<div class="row mt-5">
<div class="col-md-8 offset-md-2">
  <div class="card">
    <div class="card-header text-center">{:lang('admin/mysql/index')}</div>
    <form class="card-body pb-0" action="{:url('mysql/save','','')}" method="post">
      <div class="form-group row">
        <label class="col-sm-3 col-form-label" for="mysql_hostname">{:lang('mysql_hostname')}</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="mysql_hostname" id="mysql_hostname" value="127.0.0.1" autocomplete="off" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label" for="mysql_database">{:lang('mysql_database')}</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="mysql_database" id="mysql_database" value="daicuo" autocomplete="off" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label" for="mysql_username">{:lang('mysql_username')}</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="mysql_username" id="mysql_username" autocomplete="off" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label" for="mysql_password">{:lang('mysql_password')}</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="mysql_password" id="mysql_password" autocomplete="off" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label" for="mysql_port">{:lang('mysql_port')}</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="mysql_port" id="mysql_port" value="3306" autocomplete="off" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label" for="mysql_prefix">{:lang('mysql_prefix')}</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="mysql_prefix" id="mysql_prefix" value="dc_" autocomplete="off" required>
        </div>
      </div>
      <div class="form-group text-center">
        <button class="btn btn-dark" type="submit">{:lang('install')}</button>
      </div>
    </form>
    <div class="card-footer text-center small">
      {if config('common.apply_name')}
        {:config('common.apply_name')} V{:config('common.apply_version')}
      {else/}
        <a class="text-dark" href="{:lang('appUrl')}" target="_blank">{:lang('appName')}</a> V{:config('daicuo.version')}
      {/if}
    </div>
  </div>
</div>
</div>
</div>
</body>
</html>