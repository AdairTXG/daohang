{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>我的发布-{:config('common.site_name')}</title>
{/block}
<!-- -->
{block name="header"}
<nav class="navbar navbar-expand-md navbar-dark bg-purple">
<div class="container">
  <a class="navbar-brand" href="{$path_root}">{:config('common.site_name')}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav">
    <span class="navbar-toggler-icon"></span>
  </button>
  {if $user['user_id']}
    {assign name="navbars" value=":DcTermNavbar(['module'=>['in','common,user,daohang'],'type'=>'navbar','status'=>['in','normal,private']])" /}
  {else/}
    {assign name="navbars" value=":DcTermNavbar(['module'=>['in','common,user,daohang'],'type'=>'navbar','status'=>['in','normal,public']])" /}
  {/if}
  <div class="collapse navbar-collapse" id="nav">
    <ul class="navbar-nav ml-auto">
      {volist name="navbars" id="navbar" offset="0" length="99"}
      {if $navbar['_child']}
        <li class="nav-item dropdown {:DcDefault($module.$controll, $navbar['navs_active'], 'active')}" id="{$navbar.navs_id}">
          <a class="nav-link dropdown-toggle" href="{$navbar.navs_link}" data-toggle="dropdown">{$navbar.navs_name|DcSubstr=0,6,false}</a>
          <div class="dropdown-menu mt-0">
            {volist name="navbar._child" id="navSon"}
            <a class="dropdown-item" href="{$navSon.navs_link}" target="{$navSon.navs_target}" id="{$navSon.navs_id}">{$navSon.navs_name|DcSubstr=0,6,false}</a>
            {/volist}
          </div>
        </li>
      {else/}
        <li class="nav-item {:DcDefault($module.$controll, $navbar['navs_active'], 'active')}" id="{$navbar.navs_id}">
          <a class="nav-link" href="{$navbar.navs_link}" target="{$navbar.navs_target}">{$navbar.navs_name|DcSubstr=0,6,false}</a>
        </li>
      {/if}
      {/volist}
    </ul>
  </div>
</div>
</nav>
{/block}
<!-- -->
{block name="main"}
<div class="container pt-2 mb-3">
<div class="row dh-row">
  <div class="col-12 col-md-2 px-1">
    {include file="widget/sitebar" /}
  </div>
  <div class="col-12 col-md-10 px-1 mb-2">
    <div class="card">
      <div class="card-header">我发布的网址</div>
      <div class="card-body pb-0">
      {empty name="data"}<p>您还没有发布任何信息</p>{/empty}
      {foreach $data as $daohang}
      <h6 class="text-truncate">
        <a class="font-weight-bold" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|DcHtml}</a>
        <a class="text-purple" href="{:DcUrl('daohang/user/delete',['id'=>$daohang['info_id']])}">删除</a>
      </h6>
      <p class="text-muted small mb-1">{$daohang.info_referer|daohangReferer}</p>
      <p class="text-muted border-bottom pb-2 mb-4">
        {$daohang.info_excerpt|DcHtml}
      </p>
      {/foreach}
      </div> 
     </div>
     {gt name="last_page" value="1"}
     <div class="rounded bg-white mt-2 pt-3 d-md-none d-flex justify-content-center">{:DcPageSimple($current_page, $last_page, $pagePath)}</div>
     <div class="rounded bg-white mt-2 pt-3 d-none d-md-flex justify-content-md-center">{:DcPage($current_page, $per_page, $total, $pagePath)}</div>
     {/gt}
  </div> <!--col-12 end-->
</div> <!-- row end-->
</div> <!-- /container -->
{/block}
<!-- -->
{block name="footer"}{include file="widget/footer" /}{/block}