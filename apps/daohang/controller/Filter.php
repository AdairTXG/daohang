<?php
namespace app\daohang\controller;

use app\daohang\controller\Front;

class Filter extends Front
{
    //继承上级
    public function _initialize()
    {
        $this->requestCheck();
        //请求过滤
        $this->request->filter('trim,strip_tags,htmlspecialchars');
        //继承上级
        parent::_initialize();
    }
    
    //首页
    public function index()
    {
        return $this->detail();
    }
    
    //空操作
    public function _empty($action='')
    {
        //模型验证
        $this->actionCheck($action);
        //查询数据
        return $this->detail();
    }
    
    //公共操作
    private function detail()
    {
        //过滤空值
        $this->query        = DcArrayEmpty($this->query);
        //扩展字段列表
        $fieldsMeta         = DcArrayFilter($this->query, daohangMetaKeys('detail',NULL));
        //固定参数
        $info = [];
        $info['termId']     = intval($this->query['termId']);
        $info['termSlug']   = str_replace('termSlug','',$this->query['termSlug']);
        $info['termName']   = str_replace('termName','',$this->query['termName']);
        $info['pageSize']   = daohangLimit($this->query['pageSize']);
        $info['pageNumber'] = $this->site['page'];
        $info['sortName']   = $this->sortName($this->query['sortName']);
        $info['sortOrder']  = $this->sortOrder($this->query['sortOrder']);
        //筛选参数
        $info['pageFilter'] = DcArrayEmpty(DcArrayArgs($fieldsMeta,array_merge($info,['pageNumber'=>1])));
        //分页PATH
        $info['pagePath']   = daohangUrlFilter('daohang/filter/'.$this->request->action(),array_merge($info['pageFilter'],['pageNumber'=>'[PAGE]']) );
        //分页重置
        $info['pageReset']  = daohangUrlFilter('daohang/filter/'.$this->request->action(),[
            'termId'     => 0,
            'pageSize'   => 10,
            'pageNumber' => 1,
            'sortName'   => 'info_id',
            'sortOrder'  => 'desc'
        ]);
        //URL参数
        $info['query'] = $this->query;
        //分页大小列表
        $info['pageSizes'] = [
            '10'  => 10,
            '20'  => 20,
            '30'  => 30,
            '50'  => 50,
            '100' => 100,
        ];
        //排序字段列表
        $info['sortNames'] = [
            'info_views'       => lang('info_views'),
            'info_update_time' => lang('info_update_time'),
            'info_order'       => lang('info_order'),
            'info_up'          => lang('info_up'),
            'info_id'          => 'ID',
        ];
        //排序方式列表
        $info['sortOrders'] = [
            'desc' => lang('daohang/filter/order_desc'),
            'asc'  => lang('daohang/filter/order_asc'),
        ];
        //所有分类列表
        $terms = daohangCategorySelect([
            'cache'    => true,
            'status'   => 'normal',
            'result'   => 'array',
            'action'   => $this->request->action(),
            'field'    => 'term_id,term_slug,term_name',
            'with'     => '',
            'limit'    => 0,
            'page'     => 0,
            'sort'     => 'term_count desc,term_id',
            'order'    => 'desc',
        ]);
        foreach($terms as $key=>$value){
            $info['termIds'][$value['term_id']] = $value['term_name'];
            $info['termSlugs'][$value['term_slug']] = $value['term_name'];
        }
        //数据查询参数
        $args = [];
        $args['cache']      = true;
        $args['status']     = 'normal';
        $args['limit']      = $info['pageSize'];
        $args['page']       = $info['pageNumber'];
        $args['sort']       = $info['sortName'];
        $args['order']      = $info['sortOrder'];
        //按META字段筛选
        $args['meta_query'] = daohangMetaQuery($this->query);
        //按META字段排序
        if( !in_array($args['sort'],['info_id','info_parent','info_order','info_views','info_hits','info_create_time','info_update_time']) ){
            $args['meta_key'] = $args['sort'];
            $args['sort']     = 'meta_value_num';
        }
        //分类ID限制
        if($info['termId']){
            $args['term_id'] = ['eq',$info['termId']];
        }
        //分类别名限制
        if($info['termSlug']){
            $args['term_slug'] = ['eq',$info['termSlug']];
        }
        //分类名称限制
        if($info['termName']){
            $args['term_name'] = ['eq',$info['termName']];
        }
        //数据查询
        if( $item = daohangSelect($args) ){
            $this->assign($item);
        }
        //当前分类
        if( $info['termId'] ){
            $term = daohangCategoryId($info['termId']);
        }elseif( $info['termSlug'] ){
            $term = daohangCategorySlug($info['termSlug']);
        }elseif( $info['termName'] ){
            $term = daohangCategoryName($info['termName']);
        }
        $this->assign($term);
        //变量赋值
        $this->assign($info);
        //加载模板
        return $this->fetchTpl($this->site['action']);
    }
    
    //排序字段
    private function sortName($sortName='')
    {
        $result = [];
        foreach(daohangMetaList('detail',NULL) as $key=>$value){
            if($value['sort']){
                array_push($result,$key);
            }
        }
        if( in_array($sortName,array_merge($result,['info_up','info_down','info_id','info_parent','info_order','info_views','info_hits','info_create_time','info_update_time'])) ){
            return $sortName;
        }
        return 'info_update_time';
    }
    
    //排序方式
    private function sortOrder($sortOrder='')
    {
        if( in_array($sortOrder,['desc','asc']) ){
            return $sortOrder;
        }
        return 'desc';
    }
}