{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("admin/daohang/seo/index")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  {:lang("admin/daohang/seo/index")}
  <small class="text-muted">[siteName]，[siteDomain]，[pageNumber]，[searchText]</small>
</h6>
{:DcBuildForm([
    'name'     => 'daohang/seo/index',
    'class'    => 'bg-white py-2',
    'action'   => DcUrlAddon(['module'=>'daohang','controll'=>'seo','action'=>'update']),
    'method'   => 'post',
    'ajax'     => true,
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'disabled' => false,
    'callback' => false,
    'items'    => $items,
])}
{/block}