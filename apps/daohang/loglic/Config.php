<?php
namespace app\daohang\loglic;

class Config
{
    public function fields($data=[])
    {
        $themes = DcThemeOption('daohang');
    
        $items  = [
            'theme' => [
                'type'        =>'select', 
                'value'       => config('daohang.theme'), 
                'option'      => $themes,
                'tips'        => lang('admin/daohang/placeholder/theme'),
            ],
            'theme_wap' => [
                'type'        => 'select',
                'value'       => config('daohang.theme_wap'),
                'option'      => $themes,
                'tips'        => lang('admin/daohang/placeholder/theme_wap'),
            ],
            'slug_first' => [
                'type'        => 'select',
                'value'       => config('daohang.slug_first'),
                'option'      => [0=>lang('close'),1=>lang('open')],
                'tips'        => lang('admin/daohang/placeholder/slug_first'),
            ],
            'jump_page' => [
                'type'        => 'select',
                'value'       => config('daohang.jump_page'),
                'option'      => [0=>lang('close'),1=>lang('open')],
                'tips'        => lang('admin/daohang/placeholder/jump_page'),
            ],
            'publish_save' => [
                'type'        => 'select',
                'value'       => config('daohang.publish_save'),
                'option'      => ['off'=>lang('close'),'on'=>lang('open')],
                'tips'        => lang('admin/daohang/placeholder/publish_save'),
            ],
            'value_login' => [
                'type'        => 'select',
                'value'       => config('daohang.value_login'),
                'option'      => [0=>lang('close'),1=>lang('open')],
                'tips'        => lang('admin/daohang/placeholder/value_login'),
            ],
            'score_fast' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.score_fast')),
            ],
            'request_max' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.request_max')),
            ],
            'search_interval' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.search_interval')),
            ],
            'search_hot' => [
                'type'        => 'text',
                'value'       => config('daohang.search_hot'),
            ],
            'search_list' => [
                'type'        => 'text',
                'value'       => config('daohang.search_list'),
                'rows'        => 5,
            ],
            'label_strip' => [
                'type'        => 'text',
                'value'       => config('daohang.label_strip'),
            ],
            'post_pwd' => [
                'type'        => 'text',
                'value'       => config('daohang.post_pwd'),
            ],
            'hr_1' => [
                'type'        => 'html',
                'value'       => '<hr>',
            ],
            'limit_index_web' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.limit_index_web')),
                'placeholder' => lang('admin/daohang/placeholder/limit_index'),
            ],
            'limit_index_category' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.limit_index_category')),
                'placeholder' => lang('admin/daohang/placeholder/limit_index'),
            ],
            'limit_index_tag' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.limit_index_tag')),
                'placeholder' => lang('admin/daohang/placeholder/limit_index'),
            ],
            'limit_index_hot' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.limit_index_hot')),
                'placeholder' => lang('admin/daohang/placeholder/limit_index'),
            ],
            'limit_category' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.limit_category')),
            ],
            'limit_tag' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.limit_tag')),
            ],
            'limit_search' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.limit_search')),
            ],
            'limit_sitemap' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.limit_sitemap')),
            ],
            'limit_tag_input' => [
                'type'        => 'number',
                'value'       => intval(config('daohang.limit_tag_input')),
            ],
            'hr_2' => [
                'type'        => 'html',
                'value'       => '<hr>',
            ],
            'plus_action' => [
                'type'        => 'text',
                'value'       => config('daohang.plus_action'),
            ],
            'type_option' => [
                'type'        => 'json',
                'value'       => DcEmpty(config('daohang.type_option'),json_encode(['index'=>'标准'])),
                'rows'        => 5,
            ],
        ];
        
        foreach($items as $key=>$value){
            $items[$key]['title']       = lang('admin/daohang/title/'.$key);
            if(!isset($value['placeholder'])){
                $items[$key]['placeholder'] = lang('admin/daohang/placeholder/'.$key);
            }
        }
        
        return $items;
    }
}