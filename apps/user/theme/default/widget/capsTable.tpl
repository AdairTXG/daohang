<table class="table table-bordered table-striped table-hover bg-white">
  <thead>
    <tr class="text-center">
      {volist name=":end(userCapsTable())" id="value"}
      <th scope="col">{:lang($key)}</th>
      {/volist}
    </tr>
  </thead>
  <tbody>
    {volist name=":userCapsTable()" id="value"}
      <tr>
        {foreach $value as $td}
          <td>{$td|lang}</td>
        {/foreach}
      </tr>
    {/volist}
  </tbody>
</table>