<?php
namespace app\daohang\loglic;

class Info 
{
    public function fields($data=[])
    {
        $action = DcEmpty($data['info_action'],'index');
        
        $categorys = DcTermCheck([
            'module' => ['eq','daohang'],
            'action' => ['eq',$action],
        ]);
        
        $fields = [
            'html_1' => [
                'order'           => 0,
                'type'            => 'html',
                'value'           => '<div class="row"><div class="col-12 col-md-1 order-1">',
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'info_action' => [
                'order'           => 0,
                'type'            => 'hidden',
                'value'           => $action,
            ],
            'info_controll' => [
                'order'           => 0,
                'type'            => 'hidden',
                'value'           => 'detail',
            ],
            'info_module' => [
                'order'           => 0,
                'type'            => 'hidden',
                'value'           => 'daohang',
            ],
            'info_user_id' => [
                'order'           => 0,
                'type'            => 'hidden',
                'value'           => DcUserCurrentGetId(),
            ],
            'category_id' => [
                'order'           => 0,
                'type'            => 'checkbox',
                'value'           => $data['category_id'],
                'option'          => $categorys,
                'class_right'     => 'col-12 pre-scrollable',
                'data-filter'     => true,
                'data-type'       => 'select',
                'data-option'     => $this->idOption($categorys),
            ],
            'category_names' => [
                'order'           => 4,
                'data-title'      => lang('daohang/title/category_id'),
                'data-visible'    => true,
                'data-width'      => 150,
                'data-class'      => 'text-wrap',
                'data-align'      => 'left',
            ],
            'html_2' => [
                'order'           => 0,
                'type'            => 'html',
                'value'           => '</div><div class="col-12 col-md-8 order-2">',
            ],
            'info_id' => [
                'order'           => 1,
                'type'            => 'hidden',
                'value'           => $data['info_id'],
                'data-visible'    => true,
                'data-title'      => 'id',
                'data-sortable'   => true,
                'data-width'      => 80,
            ],
            'info_name' => [
                'order'           => 2,
                'type'            => 'text',
                'value'           => $data['info_name'],
                'data-visible'    => true,
                'data-escape'     => true,
                'data-class'      => 'text-wrap',
                'data-align'      => 'left',
            ],
            'info_slug' => [
                'order'           => 0,
                'type'            => 'text',
                'value'           => $data['info_slug'],
            ],
            'info_referer' => [
                'order'           => 3,
                'type'            => 'referer',
                'value'           => $data['info_referer'],
                'data-visible'    => true,
                'data-escape'     => true,
                'data-class'      => 'text-wrap',
                'data-align'      => 'left',
                'data-title'      => lang('daohang/title/info_referer'),
            ],
            'tag_name' => [
                'order'           => 0,
                'type'            => 'tags',
                'value'           => implode(',',$data['tag_name']),
                'option'          => daohangTags(config('daohang.limit_tag_input'), $action),
                'placeholder'     => false,
                'class_tags'      => 'form-text pt-1',
                'class_tags_list' => 'text-purple mr-2',
            ],
            'info_excerpt' => [
                'order'           => 0,
                'type'            => 'textarea',
                'value'           => $data['info_excerpt'],
                'rows'            => 3,
            ],
            'info_content' => [
                'order'           => 0,
                'type'            => 'editor',
                'value'           => $data['info_content'],
                'rows'            => 12,
                'height'          => '12rem',
            ],
            'info_title' => [
                'order'           => 0,
                'type'            => 'text',
                'value'           => $data['info_title'],
                'placeholder'     => false,
            ],
            'info_keywords' => [
                'order'           => 0,
                'type'            => 'text',
                'value'           => $data['info_keywords'],
                'placeholder'     => false,
            ],
            'info_description' => [
                'order'           => 0,
                'type'            => 'text',
                'value'           => $data['info_description'],
                'placeholder'     => false,
            ],
            'html_3' => [
                'order'           => 0,
                'type'            => 'html',
                'value'           => '</div><div class="col-12 col-md-3 order-3">',
            ],
            'info_status' => [
                'order'           => 0,
                'type'            => 'select',
                'value'           => $data['info_status'],
                'option'          => $this->statusOption(),
                'data-filter'     => true,
            ],
            'info_status_text' => [
                'order'           => 5,
                'data-visible'    => true,
                'data-title'      => lang('daohang/title/info_status'),
                'data-width'      => 90,
            ],
            'info_type' => [
                'order'           => 6,
                'type'            => 'select',
                'value'           => $data['info_type'],
                'option'          => $this->typeOption(),
                'data-filter'     => true,
                'data-option'     => array_merge($this->typeOption(),[''=>'------']),
            ],
            'info_type_text' => [
                'order'           => 7,
                'data-visible'    => true,
                'data-title'      => lang('daohang/title/info_type'),
                'data-width'      => 90,
            ],
            'info_color' => [
                'order'           => 0,
                'type'            => 'select',
                'value'           => $data['info_color'],
                'option'          => [
                    'text-dark'      => 'text-dark',
                    'text-danger'    => 'text-danger',
                    'text-success'   => 'text-success',
                    'text-primary'   => 'text-primary',
                    'text-info'      => 'text-info',
                    'text-secondary' => 'text-secondary',
                    'text-muted'     => 'text-muted',
                    'text-light'     => 'text-light',
                ],
            ],
            'image_ico' => [
                'order'           => 0,
                'type'            => 'image',
                'value'           => $data['image_ico'],
            ],
            'image_level' => [
                'order'           => 0,
                'type'            => 'image',
                'value'           => $data['image_level'],
            ],
            'info_tpl' => [
                'order'           => 0,
                'type'            => 'text',
                'value'           => $data['info_tpl'],
            ],
            'info_order' => [
                'order'           => 8,
                'type'            => 'number',
                'value'           => intval($data['info_order']),
                'order'           => 93,
                'data-sortable'   => true,
                'data-visible'    => true,
                'data-width'      => 80,
            ],
            'info_views' => [
                'order'           => 9,
                'type'            => 'number',
                'value'           => intval($data['info_views']),
                'data-sortable'   => true,
                'data-visible'    => true,
                'data-width'      => 80,
            ],
            'info_hits' => [
                'order'           => 10,
                'type'            => 'number',
                'value'           => intval($data['info_hits']),
                'data-sortable'   => true,
                'data-visible'    => true,
                'data-width'      => 80,
            ],
            'info_up' => [
                'order'           => 11,
                'type'            => 'number',
                'value'           => intval($data['info_up']),
                'data-sortable'   => true,
                'data-visible'    => true,
                'data-width'      => 80,
            ],
            'info_down' => [
                'order'           => 12,
                'type'            => 'number',
                'value'           => intval($data['info_down']),
                'order'           => 97,
                'data-sortable'   => true,
                'data-visible'    => true,
                'data-width'      => 80,
            ],
            'info_domain' => [
                'order'           => 0,
                'type'            => 'text',
                'value'           => $data['info_domain'],
            ],
            'info_unique' => [
                'order'           => 0,
                'type'            => 'text',
                'value'           => $data['info_unique'],
            ],
            'user_name' => [
                'order'           => 98,
                'data-visible'    => true,
                'data-escape'     => true,
                'data-width'      => 100,
            ],
            'info_update_time' => [
                'order'           => 99,
                'data-sortable'   => true,
                'data-visible'    => true,
                'data-width'      => 120,
            ],
            'html_4' => [
                'order'           => 0,
                'type'            => 'html',
                'value'           => '</div></div>',
            ]
        ];
        //动态扩展字段（可精确到操作名）
        $customs = daohangMetaList('detail', DcEmpty($data['info_action'],'index'));
        //合并所有字段
        if($customs){
            $fields = DcArrayPush($fields, DcFields($customs, $data), 'html_3');
        }
        //公共属性
        foreach($fields as $key=>$value){
            if(!isset($value['title'])){
                $fields[$key]['title'] = lang('daohang/title/'.$key);
            }
            if(!isset($value['placeholder'])){
                $fields[$key]['placeholder'] = lang('daohang/placeholder/'.$key);
            }
        }
        //返回所有表单字段
        return $fields;
    }
    
    //表单字段处理
    public function postSet($userId=1,$data=[])
    {
        //合法字段
        //$fields = array_merge(daohangFields(),['tag_name','tag_id','category_id']);
        $fields = array_merge(daohangMetaKeys('detail', NULL),['info_id','info_name','info_action','info_content','tag_name','tag_id','category_id']);
        //字段过滤
        $data = DcArrayFilter($data, $fields);
        //默认属性
        $data['info_views']       = 1;
        $data['info_hits']        = 1;
        $data['info_up']          = 1;
        $data['info_down']        = 1;
        $data['info_user_id']     = $userId;
        $data['info_type']        = 'index';
        $data['info_status']      = 'hidden';
        $data['info_tpl']         = false;
        $data['info_module']      = 'daohang';
        $data['info_controll']    = 'detail';
        //内容模型
        if(!$data['info_action']){
            $data['info_action'] = 'index';
        }else{
            if($data['info_action'] != 'index'){
                if(!in_array($data['info_action'],explode(',',config('daohang.plus_action')))){
                    $data['info_action']  = 'index';
                }
            }
        }
        return $data;
    }
    
    //获取网站TKD
    public function curl($url='https://www.daicuo.org')
    {
        if(!$url){
            return false;
        }
        if(!parse_url($url, PHP_URL_SCHEME)){
            return false;
        }
        $html = DcCurl('windows', 10, $url, '', 'https://www.baidu.com');
        if(!$html){
            return false;
        }
        //过滤连续空白
        $html = daohangTrim($html);
        //表单数据
        $data = [];
        $data['info_title']       = DcPregMatch('<title>([\s\S]*?)<\/title>', $html);
        $data['info_keywords']    = str_replace('，',',',DcPregMatch('<meta name="keywords" content="([\s\S]*?)"', $html));
        $data['info_description'] = DcPregMatch('<meta name="description" content="([\s\S]*?)"', $html);
        $data['image_ico']        = DcPregMatch('rel="apple-touch-icon" href="([\s\S]*?)"', $html);
        if(!$data['image_ico']){
            $data['image_ico']    = DcPregMatch('rel="icon" href="([\s\S]*?)"', $html);
        }
        if($data['info_title']){
            $data['info_name']    = $data['info_title'];
        }
        if($data['info_description']){
            $data['info_content'] = $data['info_description'];
        }
        //返回结果
        return $data;
    }
    
    //网址导航选项
    public function typeOption()
    {
        $types = json_decode(config('daohang.type_option'),true);
        //合并初始值
        return DcArrayArgs($types, [
            'index'     => lang('daohang/option/info_type_index'),
            'fast'      => lang('daohang/option/info_type_fast'),
            'recommend' => lang('daohang/option/info_type_recommend'),
            'head'      => lang('daohang/option/info_type_head'),
        ]);
    }
    
    //状态选项
    public function statusOption()
    {
        return [
            'normal'  => lang('normal'),
            'hidden'  => lang('hidden'),
            'private' => lang('private'),
        ];
    }
    
    //初始筛选的option
    private function idOption($array=[])
    {
        $array[0] = '------';
        return $array;
    }
}