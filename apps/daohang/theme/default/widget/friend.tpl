{if function_exists('friendSelect')}
<div class="container mb-2">
  <div class="card">
    <div class="card-header px-2 d-flex flex-row justify-content-between align-items-center">
      <span><i class="fa fa-fw fa-link mr-1 text-purple"></i>友情链接</span>
      <a class="text-muted small" href="{:daohangUrl('friend/index/index')}" target="_self">全部>></a>
    </div>
    <div class="card-body row py-1">
      {volist name=":friendSelect(['status'=>'normal','limit'=>'30','sort'=>'info_order','order'=>'desc'])" id="friend"}
      <div class="col-6 col-md-2 my-1">
        <a class="text-dark" href="{$friend.friend_referer|daohangReferer}" target="_blank" data-id="{$friend.info_id}" data-type="link">{$friend.info_name|daohangSubstr=0,8,false}</a>
      </div>
      {/volist}
    </div> 
  </div>
</div>
{/if}