<?php
return [
    'admin_index_count' => [
        'app\\user\\behavior\\Hook',
    ],
    'table_build' => [
        'app\\user\\behavior\\Hook',
    ],
    'user_register_before' => [
        'app\\user\\behavior\\Hook',
    ],
    'user_register_after' => [
        'app\\user\\behavior\\Hook',
    ],
    'user_login_after' => [
        'app\\user\\behavior\\Hook',
    ],
    /*'user_login_before' => [
        'app\\user\\behavior\\Hook',
    ],
    'user_score_index' => [
        'app\\user\\behavior\\Hook',
    ]
    */
];