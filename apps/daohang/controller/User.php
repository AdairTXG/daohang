<?php
namespace app\daohang\controller;

use app\daohang\controller\Front;

class User extends Front
{
    protected $auth = [
         'check'       => true,
         'none_login'  => '',
         'none_right'  => '*',
         'error_login' => 'user/login/index',
         'error_right' => 'user/center/index',
    ];
    
	public function _initialize()
    {
		parent::_initialize();
	}
    
	public function index()
    {
        $item = daohangSelect([
            'cache'   => false,
            'user_id' => $this->site['user']['user_id'],
            'field'   => 'info_id,info_name,info_slug,info_type,info_action,info_excerpt',
            'limit'   => 10,
            'page'    => $this->site['page'],
            'sort'    => 'info_id',
            'order'   => 'desc',
        ]);
        if($item){
            $this->assign($item);
        }
        $this->assign('pagePath',DcUrl('daohang/user/index',['pageNumber'=>'[PAGE]']));
		return $this->fetch();
	}
    
    public function delete()
    {
        $result = \daicuo\Info::delete_all([
            'info_user_id' => ['eq',$this->site['user']['user_id']],
            'info_id'      => ['eq',input('id/d',0)],
        ]);
        $this->success(lang('success'));
    }
}