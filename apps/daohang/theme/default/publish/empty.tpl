{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:daohangSeo(lang('daohang/publish/'.$action.'/title'))}－{:config('common.site_name')}</title>
<meta name="keywords" content="{:daohangSeo(lang('daohang/publish/'.$action.'/keywords'))}" />
<meta name="description" content="{:daohangSeo(lang('daohang/publish/'.$action.'/description'))}" />
{/block}
<!-- -->
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
<div class="rounded bg-white pt-4 px-3 pb-3">
<!---->
<h2 class="text-center mb-3">免费发布</h2>
<h6 class="text-center small mb-3">免费发布的信息需要管理员审核通过后才能显示、如需免审核服务请点击“<a class="text-purple" href="{:DcUrl('daohang/fast/'.$action)}">这里</a>”。</h6>
{:DcBuildForm([
    'name'     => 'daohang/publish/index',
    'class'    => 'w-100',
    'action'   => daohangUrl('daohang/publish/save'),
    'method'   => 'post',
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'ajax'     => true,
    'disabled' => false,
    'callback' => false,
    'data'     => false,
    'items'    => $fields,
    'submit_class' => 'btn btn-purple',
    'reset_class'  => 'btn btn-dark',
])}
</div>
</div>
{/block}
{block name="footer"}{include file="widget/footer" /}{/block}