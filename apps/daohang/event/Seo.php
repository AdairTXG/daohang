<?php
namespace app\daohang\event;

use think\Controller;

class Seo extends Controller
{
    public function _initialize()
    {
        $this->query = $this->request->param();
        
        parent::_initialize();
    }
    
    public function index()
    {
        $items = model('daohang/Seo','loglic')->fields($this->query);
        
        $this->assign('items', DcFormItems($items));
        
        return $this->fetch('daohang@seo/index');
    }
    
    public function update(){
        $status = daohangConfigSave(input('post.'));
		if( !$status ){
		    $this->error(lang('fail'));
        }
        //处理伪静态路由
        $this->rewriteRoute(input('post.'));
        //返回结果
        $this->success(lang('success'));
    }
    
    //配置伪静态
    private function rewriteRoute($post)
    {
        //批量删除路由伪静态
        \daicuo\Op::delete_all([
            'op_name'     => ['eq','site_route'],
            'op_module'   => ['eq','daohang'],
        ]);
        //批量添加路由伪静态
        $result = \daicuo\Route::save_all([
            [
                'rule'        => $post['rewrite_index'],
                'address'     => 'daohang/index/index',
                'method'      => '*',
                'op_module'   => 'daohang',
            ],
            [
                'rule'        => $post['rewrite_category'],
                'address'     => 'daohang/category/index',
                'method'      => '*',
                'op_module'   => 'daohang',
            ],
            [
                'rule'        => $post['rewrite_tag'],
                'address'     => 'daohang/tag/index',
                'method'      => '*',
                'op_module'   => 'daohang',
            ],
            [
                'rule'        => $post['rewrite_search'],
                'address'     => 'daohang/search/index',
                'method'      => '*',
                'op_module'   => 'daohang',
            ],
            [
                'rule'        => $post['rewrite_detail'],
                'address'     => 'daohang/detail/index',
                'method'      => '*',
                'op_module'   => 'daohang',
            ],
            [
                'rule'        => $post['rewrite_filter'],
                'address'     => 'daohang/filter/index',
                'method'      => '*',
                'op_module'   => 'daohang',
            ],
        ]);
        //清理全局缓存
        DcCache('route_all', null);
    }
}