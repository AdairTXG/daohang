<?php
namespace app\daohang\controller;

use app\daohang\controller\Front;

class Page extends Front
{

    public function _initialize()
    {
        $this->requestCheck();
        
        parent::_initialize();
    }
    
    //首页
    public function index()
    {
        return $this->fetch();
    }
    
    //分类
    public function types()
    {
        $info = [];
        $info['seoTitle']       = daohangSeo(config('daohang.category_title'));
        $info['seoKeywords']    = daohangSeo(config('daohang.category_keywords'));
        $info['seoDescription'] = daohangSeo(config('daohang.category_description'));
        $this->assign($info);
        return $this->fetch();
    }
    
    //标签
    public function tags()
    {
        $info = [];
        $info['pagePath']       = daohangUrl('daohang/page/tags',['pageNumber'=>'[PAGE]']);
        $info['pageSize']       = DcEmpty(intval(config('daohang.page_size')),20);
        $info['pageNumber']     = $this->site['page'];
        $info['sortName']       = 'term_count desc,term_id';
        $info['sortOrder']      = 'desc';
        $info['seoTitle']       = daohangSeo(config('daohang.tag_title'),$this->site['page']);
        $info['seoKeywords']    = daohangSeo(config('daohang.tag_keywords'),$this->site['page']);
        $info['seoDescription'] = daohangSeo(config('daohang.tag_description'),$this->site['page']);
        $this->assign($info);
        return $this->fetch();
    }
    
    //最新
    public function news()
    {
        return $this->fetch();
    }
    
    //热门
    public function views()
    {
        return $this->fetch();
    }
    
    //推荐
    public function recommend()
    {
        return $this->fetch();
    }
    
    //快审
    public function fast()
    {
        return $this->fetch();
    }
    
    //头条
    public function head()
    {
        return $this->fetch();
    }
    
    //空操作
    public function _empty($action='')
    {
        $tplPath = './'.$this->site['path_view'].$this->site['controll'].'/'.$action.'.tpl';
        if(is_file($tplPath)){
            return $this->fetch($action);
        }
        return '扩展内容模型需自行开发模板（'.$tplPath.'）';
    }
}