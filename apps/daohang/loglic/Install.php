<?php
namespace app\daohang\loglic;

class Install
{
    //一键安装回调
    public function mysql()
    {
        \daicuo\Apply::install('user','install');
        
        \think\Cache::clear();//依赖插件安装后需要重加载应用列表
        
        \daicuo\Apply::install('friend','install');
        
        \daicuo\Apply::install('adsense','install');
        
        \daicuo\Apply::install('pay','install');
        
        \daicuo\Apply::install('daohang','install');
        
        model('daohang/Upgrade','loglic')->pack();
        
        $this->mysqlRrewrite();
        
        return true;
    }
    
    //设置首页路由
    private function mysqlRrewrite()
    {
        //公共
        \daicuo\Op::write([
            'site_name'  => '呆错网址导航系统',
            'url_suffix' => '.html',
        ], 'common', 'config', 'system', 0, 'yes');
        //配置
        \daicuo\Op::write([
            'rewrite_index' => '/',
        ], 'daohang', 'config', 'system', 0, 'yes');
        //删除
        db('op')->where([
            'op_name'  => 'site_route',
            'op_value' => ['like','a:5:{s:4:"rule";s:3:"dh$"%'],
        ])->delete();
        //首页
        return model('common/Route','loglic')->index('daohang/index/index', 'daohang');
    }
    
    //初始配置
    public function config()
    {
        return model('common/Config','loglic')->install([
            'theme'                => 'default',
            'theme_wap'            => 'default',
            'slug_first'           => 1,
            'jump_page'            => 1,
            'publish_save'         => 'on',
            'value_login'          => 0,
            'score_fast'           => 10,
            'request_max'          => 0,
            'search_list'          => 'daohang/search/baidu,daohang/search/sogou,daohang/search/toutiao,daohang/search/bing,daohang/search/so',
            'search_interval'      => 0,
            'search_hot'           => '呆错网址导航系统,呆错后台管理框架,呆错文章管理系统,daicuo,feifeicms',
            'label_strip'          => '<p>,<i>,<font>,<strong>',
            'post_pwd'             => '',
            'limit_index_web'      => 60,
            'limit_index_category' => 30,
            'limit_index_tag'      => 60,
            'limit_index_hot'      => 20,
            'limit_category'       => 20,
            'limit_tag'            => 30,
            'limit_search'         => 10,
            'limit_sitemap'        => 100,
            'limit_tag_input'      => 10,
            'plus_action'          => '',
            'type_option'          => false,
            'rewrite_index'        => 'dh$',
            'rewrite_category'     => 'dhtype/:slug/[:pageNumber]',
            'rewrite_tag'          => 'dhtag/:slug/[:pageNumber]',
            'rewrite_search'       => 'dhsearch/[:searchText]/[:pageNumber]',
            'rewrite_filter'       => 'dhfilter/<termId>-<pageSize>-<pageNumber>-<sortName>-<sortOrder>',
            'rewrite_detail'       => 'dhinfo/:slug$',
            'index_title'          => '呆错网址导航系统',
            'index_keywords'       => 'PHP导航系统,免费导航系统,开源导航系统',
            'index_description'    => '呆错导航系统是一款免费开源的分类导航建站系统，安全、便捷、高效、是您的建站首选。',
            'search_title'         => '与[searchText]有关的网站第[pageNumber]页',
            'search_keywords'      => '搜索[searchText]',
            'search_description'   => '在[siteName]搜索到与[searchText]有关的结果如下。',
            'category_title'       => '栏目分类',
            'category_keywords'    => '网站分类',
            'category_description' => '[siteName]所有网址导航分类。',
            'tag_title'            => '标签第[pageNumber]页',
            'tag_keywords'         => '标签列表',
            'tag_description'      => '[siteName]所有标签。',
            'publish_title'        => '免费收录网站',
            'publish_keywords'     => '免费收录网站',
            'publish_description'  => '[siteName]提供免费收录网站服务。',
            'fast_title'           => '免审核收录网站',
            'fast_keywords'        => '付费免审核收录网站',
            'fast_description'     => '[siteName]提供付费免审核收录网站服务。',
        ],'daohang');
    }
    
    //初始字段
    public function field()
    {
        config('common.validate_name', false);
        
        return model('common/Field','loglic')->install([
            [
                'op_name'     => 'info_color',
                'op_value'    => json_encode([
                    'type'         => 'text',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'daohang',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'image_ico',
                'op_value'    => json_encode([
                    'type'         => 'image',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'daohang',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'image_level',
                'op_value'    => json_encode([
                    'type'         => 'image',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'daohang',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'info_up',
                'op_value'    => json_encode([
                    'type'         => 'number',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'daohang',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'info_down',
                'op_value'    => json_encode([
                    'type'         => 'number',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'daohang',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'info_referer',
                'op_value'    => json_encode([
                    'type'         => 'text',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'daohang',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'info_domain',
                'op_value'    => json_encode([
                    'type'         => 'text',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'daohang',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'info_unique',
                'op_value'    => json_encode([
                    'type'         => 'text',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'daohang',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'info_tpl',
                'op_value'    => json_encode([
                    'type'         => 'text',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'daohang',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
        ]);
    }
    
    //伪静态规则
    public function route()
    {
        config('common.validate_name', false);
        
        return model('common/Route','loglic')->install([
            [
                'rule'        => 'dh$',
                'address'     => 'daohang/index/index',
                'method'      => 'get',
            ],
            [
                'rule'        => 'dhtype/:slug/[:pageNumber]',
                'address'     => 'daohang/category/index',
                'method'      => 'get',
            ],
            [
                'rule'        => 'dhtag/:slug/[:pageNumber]',
                'address'     => 'daohang/tag/index',
                'method'      => 'get',
            ],
            [
                'rule'        => 'dhsearch/[:searchText]/[:pageNumber]',
                'address'     => 'daohang/search/index',
                'method'      => '*',
            ],
            [
                'rule'        => 'dhfilter/<termId>-<pageSize>-<pageNumber>-<sortName>-<sortOrder>',
                'address'     => 'daohang/filter/index',
                'method'      => 'get',
            ],
            [
                'rule'        => 'dhinfo/:slug$',
                'address'     => 'daohang/detail/index',
                'method'      => 'get',
            ],
        ],'daohang');
    }
    
    //权限规则
    public function auth()
    {
        //取消验证
        config('common.validate_name', false);
        
        //权限节点
        $caps = [
            'daohang/score/save',
            'daohang/fast/save',
            'daohang/filter/index',
            'daohang/search/common',
            'daohang/referer/index',
            'daohang/data/index',
        ];
        
        //默认数据
        $default = [
            'op_name'       => 'vip',
            'op_module'     => 'daohang',
            'op_controll'   => 'auth',
            'op_action'     => 'front',//前台权限
        ];
        
        //批量添加数据
        $dataList = [];
        foreach($caps as $key=>$value){
            array_push($dataList, DcArrayArgs(['op_value'=>$value],$default));
        }

        //调用接口
        return model('common/Auth','loglic')->install($dataList);
    }
    
    //采集规则
    public function collect()
    {
        config('common.validate_name', false);
        
        model('daohang/Collect','loglic')->write([
            'collect_name'     => '网址大全',
            'collect_url'      => 'http://api.daicuo.cc/wzdq/',
            'collect_token'    => '',
            'collect_category' => '',
        ]);
        
        model('daohang/Collect','loglic')->write([
            'collect_name'     => '演示数据',
            'collect_url'      => 'http://demo.daicuo.com/index.php',
            'collect_token'    => 'fb05aabf582499263dfc235a82629069',
            'collect_category' => '',
        ]);
        
        return true;
    }
    
    //后台菜单
    public function menu()
    {
        $result = model('common/Menu','loglic')->install([
            [
                'term_name'   => '导航',
                'term_slug'   => 'daohang',
                'term_info'   => 'fa-link',
                'term_module' => 'daohang',
                'term_order'  => 9,
            ],
        ]);
        
        $result = model('common/Menu','loglic')->install([
            [
                'term_name'   => '网站管理',
                'term_slug'   => 'daohang/admin/index',
                'term_info'   => 'fa-list',
                'term_module' => 'daohang',
                'term_order'  => 9,
            ],
            [
                'term_name'   => '采集管理',
                'term_slug'   => 'daohang/collect/index',
                'term_info'   => 'fa-cloud',
                'term_module' => 'daohang',
                'term_order'  => 8,
            ],
            [
                'term_name'   => '栏目管理',
                'term_slug'   => 'admin/category/index?parent=daohang&term_module=daohang',
                'term_info'   => 'fa-clone',
                'term_module' => 'daohang',
                'term_order'  => 7,
            ],
            [
                'term_name'   => '标签管理',
                'term_slug'   => 'admin/tag/index?parent=daohang&term_module=daohang',
                'term_info'   => 'fa-tags',
                'term_module' => 'daohang',
                'term_order'  => 6,
            ],
            [
                'term_name'   => '菜单管理',
                'term_slug'   => 'admin/navs/index?parent=daohang&navs_module=daohang',
                'term_info'   => 'fa-sitemap',
                'term_module' => 'daohang',
                'term_order'  => 5,
            ],
            [
                'term_name'   => 'SEO优化',
                'term_slug'   => 'daohang/seo/index',
                'term_info'   => 'fa-anchor',
                'term_module' => 'daohang',
                'term_order'  => 4,
            ],
            [
                'term_name'   => '字段扩展',
                'term_slug'   => 'admin/field/index?parent=daohang&op_module=daohang',
                'term_info'   => 'fa-cube',
                'term_module' => 'daohang',
                'term_order'  => 2,
            ],
            [
                'term_name'   => '语言扩展',
                'term_slug'   => 'admin/lang/index?parent=daohang&op_module=daohang',
                'term_info'   => 'fa-commenting',
                'term_module' => 'daohang',
                'term_order'  => 1,
            ],
            [
                'term_name'   => '频道设置',
                'term_slug'   => 'daohang/config/index',
                'term_info'   => 'fa-gear',
                'term_module' => 'daohang',
                'term_order'  => -1,
            ],
        ],'导航');
        
        return true;
    }
    
    //前台菜单
    public function navs()
    {
        return model('common/Navs','loglic')->install([
            //导航栏
            [
                'navs_type'       => 'navbar',
                'navs_name'       => '导航首页',
                'navs_info'       => '呆错网址导航系统首页',
                'navs_url'        => 'daohang/index/index',
                'navs_status'     => 'normal',
                'navs_active'     => 'daohangindexindex',
                'navs_target'     => '_self',
                'navs_order'      => 103,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
            [
                'navs_type'       => 'navbar',
                'navs_name'       => '免费收录',
                'navs_info'       => '免费发布网站',
                'navs_url'        => 'daohang/publish/index',
                'navs_status'     => 'normal',
                'navs_active'     => 'daohangpublishindex',
                'navs_target'     => '_self',
                'navs_order'      => 102,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
            [
                'navs_type'       => 'navbar',
                'navs_name'       => '快审服务',
                'navs_info'       => '免审核发布网站',
                'navs_url'        => 'daohang/fast/index',
                'navs_status'     => 'normal',
                'navs_active'     => 'daohangfastindex',
                'navs_target'     => '_self',
                'navs_order'      => 101,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
            //侧边栏
            [
                'navs_type'       => 'sitebar',
                'navs_name'       => '我的网址',
                'navs_info'       => '我发布的网址',
                'navs_url'        => 'daohang/user/index',
                'navs_status'     => 'private',
                'navs_active'     => 'daohanguserindex',
                'navs_target'     => '_self',
                'navs_order'      => 9,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
            //底部链接
            [
                'navs_type'       => 'link',
                'navs_name'       => '网站地图',
                'navs_info'       => '网站地图',
                'navs_url'        => 'daohang/page/index',
                'navs_status'     => 'normal',
                'navs_active'     => 'daohangpageindex',
                'navs_target'     => '_self',
                'navs_order'      => 0,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
            [
                'navs_type'       => 'link',
                'navs_name'       => 'SiteMap',
                'navs_info'       => '呆错导航系统sitemap',
                'navs_url'        => 'daohang/sitemap/index',
                'navs_status'     => 'normal',
                'navs_active'     => 'daohangsitemapindex',
                'navs_target'     => '_self',
                'navs_order'      => 0,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
            [
                'navs_type'       => 'link',
                'navs_name'       => '免费收录',
                'navs_info'       => '免费收录网站',
                'navs_url'        => 'daohang/publish/index',
                'navs_status'     => 'normal',
                'navs_active'     => 'daohangpublishindex',
                'navs_target'     => '_self',
                'navs_order'      => 0,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
            [
                'navs_type'       => 'link',
                'navs_name'       => '快审服务',
                'navs_info'       => '免审核发布网站',
                'navs_url'        => 'daohang/fast/index',
                'navs_status'     => 'normal',
                'navs_active'     => 'daohangfastindex',
                'navs_target'     => '_self',
                'navs_order'      => 0,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
            [
                'navs_type'       => 'link',
                'navs_name'       => '栏目分类',
                'navs_info'       => '所有栏目分类',
                'navs_url'        => 'daohang/page/types',
                'navs_status'     => 'normal',
                'navs_active'     => 'daohangpagetypes',
                'navs_target'     => '_self',
                'navs_order'      => 0,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
            [
                'navs_type'       => 'link',
                'navs_name'       => '标签列表',
                'navs_info'       => '所有标签列表',
                'navs_url'        => 'daohang/page/tags',
                'navs_status'     => 'normal',
                'navs_active'     => 'daohangpagetags',
                'navs_target'     => '_self',
                'navs_order'      => 0,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
            [
                'navs_type'       => 'link',
                'navs_name'       => '最新收录',
                'navs_info'       => '最新收录的网站',
                'navs_url'        => 'daohang/page/news',
                'navs_status'     => 'normal',
                'navs_active'     => 'daohangpagenews',
                'navs_target'     => '_self',
                'navs_order'      => 0,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
            [
                'navs_type'       => 'link',
                'navs_name'       => '网站排行',
                'navs_info'       => '人气最高的网站',
                'navs_url'        => 'daohang/page/views',
                'navs_status'     => 'normal',
                'navs_active'     => 'daohangpageviews',
                'navs_target'     => '_self',
                'navs_order'      => 0,
                'navs_parent'     => 0,
                'navs_module'     => 'daohang',
            ],
        ]);
    }
    
    //初始分类
    public function category()
    {
        //一级分类
        model('common/Category','loglic')->install([
            [
                'term_name'       => '网站大全',
                'term_slug'       => 'wzdq',
                'term_type'       => 'navbar',
                'term_order'      => 1,
                'term_module'     => 'daohang',
                'term_controll'   => 'category',
                'term_action'     => 'index',
                'term_limit'      => 0,
                'term_tpl'        => 'channel',
            ],
        ]);
        
        //二级分类
        $list = [];
        foreach(['生活服务','综合其他','休闲娱乐','教育文化','行业企业','网络科技','政府组织','购物网站','新闻媒体','交通旅游','医疗健康','体育健身'] as $key=>$value){
            $list[$key] = [
                'term_name'   => $value,
                'term_slug'   => DcPinYin($value),
                'term_info'   => '收录与'.$value.'相关的网站',
                'term_type'   => 'navbar',
                'term_module' => 'daohang',
            ];
        }
        return model('common/Category','loglic')->install($list,'网站大全');
    }
}