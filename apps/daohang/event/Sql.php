<?php
namespace app\daohang\event;

class Sql
{
    /**
    * 安装时触发
    * @return bool 只有返回true时才会往下执行
    */
	public function install()
    {
        //配置
        model('daohang/Install','loglic')->config();
        
        //字段
        model('daohang/Install','loglic')->field();
        
        //路由
        model('daohang/Install','loglic')->route();
        
        //权限
        model('daohang/Install','loglic')->auth();
        
        //采集规则
        model('daohang/Install','loglic')->collect();
        
        //后台菜单
        model('daohang/Install','loglic')->menu();
        
        //前台导航
        model('daohang/Install','loglic')->navs();
        
        //应用分类
        model('daohang/Install','loglic')->category();
        
        //清空缓存
        \think\Cache::clear();
        
        //返回结果
        return true;
	}
    
    /**
    * 升级时触发
    * @return bool 只有返回true时才会往下执行
    */
    public function upgrade()
    {
        //更新基础信息
        model('daohang/Upgrade','loglic')->status();
        
        //更新打包配置
        model('daohang/Upgrade','loglic')->pack();
        
        //清空缓存
        \think\Cache::clear();
        
        //返回结果
        return true;
    }
    
    /**
    * 卸载插件时触发
    * @return bool 只有返回true时才会往下执行
    */
    public function remove()
    {
        return model('daohang/Datas','loglic')->remove();
    }
    
    /**
    * 删除插件时触发
    * @return bool 只有返回true时才会往下执行
    */
    public function unInstall()
    {
        return model('daohang/Datas','loglic')->delete();
	}
}