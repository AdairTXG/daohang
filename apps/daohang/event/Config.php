<?php
namespace app\daohang\event;

use think\Controller;

class Config extends Controller
{
	
    public function _initialize()
    {
        $this->query = $this->request->param();
        
        parent::_initialize();
    }

	public function index()
    {
        $items = model('daohang/Config','loglic')->fields($this->query);
        
        $this->assign('items', DcFormItems($items));
        
        return $this->fetch('daohang@config/index');
	}
    
    public function update()
    {
        $status = daohangConfigSave(input('post.'));
		if(!$status){
		    $this->error(lang('fail'));
        }
        $this->success(lang('success'));
	}
}