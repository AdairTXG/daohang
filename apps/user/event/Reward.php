<?php
namespace app\user\event;

use think\Controller;

class Reward extends Controller
{
	
	public function _initialize()
    {
		parent::_initialize();
	}

	public function index()
    {
        $items = [
            'user_name' => [
                'type'        => 'text', 
                'title'       => lang('user_reward_name'),
                'class_right' => 'col-12 col-md-3',
            ],
            'user_id' => [
                'type'        => 'text', 
                'title'       => lang('user_reward_id'),
                'class_right' => 'col-12 col-md-3',
            ],
            'score_value' => [
                'type'        => 'number', 
                'value'       => 0,
                'title'       => lang('user_reward_value'),
                'tips'        => lang('user_reward_value_tips'),
                'class_right' => 'col-12 col-md-3',
            ],
        ];
        
        //变量赋值
        $this->assign('items', DcFormItems($items));
        
        return $this->fetch('user@reward/index');
	}
    
    public function update()
    {
        $post = input('post.');
        //积分数值
        if($post['score_value']==0){
            $this->error(lang('empty'));
        }elseif($post['score_value']>0){
            $logAction = 'add';
        }else{
            $logAction = 'delete';
        }
        //用户ID
        if(!$post['user_id']){
            if($post['user_name']){
                $post['user_id'] = db('user')->where('user_name',['eq',$post['user_name']])->value('user_id');
            }
        }
        if(!$post['user_id']){
            $this->error(lang('user_error_reward_empty'));
        }
        //积分增减
        if( $result = userScoreInc($post['user_id'], $post['score_value']) ){
            //积分记录
            model('user/Log','loglic')->userScore($post['user_id'], $post['score_value'], 'reward', $logAction, '127.0.0.1');
        }
        //返回结果
        if($result){
            $this->success(lang('success'));
        }else{
            $this->error(lang('fail'));
        }
    }
	
}