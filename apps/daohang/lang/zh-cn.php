<?php
//插件语言包
return [
    //重置系统
    'user_token_none'                   => '请先登录',
    
    //扩展字段
    'info_up'                           => '点赞',
    'info_down'                         => '讨厌',
    'info_color'                        => '颜色',
    'info_unique'                       => '采集标识',
    'info_domain'                       => '顶级域名',
    'info_referer'                      => '网址',
    'image_level'                       => '封面',
    'image_ico'                         => '图标',
    'info_tpl'                          => '模板',
    
    //错误信息
    'daohang/error/empty'               => '未审核或不存在',
    'daohang/error/params'              => '缺少参数',
    'daohang/error/rest'                => '请休息一下',
    'daohang/error/action'              => '内容模型未启用',
    'daohang/error/publish_off'         => '免费发布未开启',
    'daohang/error/fast_vip'            => '请升级到VIP',
    'daohang/error/score_light'         => '用户积分不足',
    'daohang/error/post_pwd_config'     => '请先在后台设置入库密码',
    'daohang/error/post_pwd_empty'      => '未找到密码字段（post_pwd）',
    'daohang/error/post_pwd_wrong'      => '采集器入库密码错误',
    'daohang/error/referer'             => '解析网址失败，需http(s)开头',
    'daohang/error/referer_empty'       => '请输入http(s)开头的网址',
    
    //内容模型
    'daohang/action/index'              => '网址',
    'daohang/action/channel'            => '频道',
    
    //日志
    'daohang/log/fast/index'            => '免审核发布网站',
    
    //按钮
    'daohang/btn/parse'                 => '解析',
    
    //表单验证
    'daohang/require/category_id'       => '请选择栏目分类',
    'daohang/require/info_content'      => '请填写网站介绍',
    'daohang/require/info_id'           => 'info_id%内容ID必须填写',
    'daohang/require/info_module'       => 'info_module%应用标识必须填写',
    'daohang/require/info_name'         => 'info_name%名称必须填写',
    'daohang/require/info_referer'      => 'info_referer%网址必须填写',
    'daohang/url/info_referer'          => 'info_referer%请输入https或http开头的网址',
    'daohang/unique/info_referer'       => 'info_referer%网址已存在',
    'daohang/unique/term_name'          => 'term_name%名称已存在',
    
    //表单标题
    'daohang/title/category_id'         => '分类',
    'daohang/title/info_name'           => '名称',
    'daohang/title/info_slug'           => '别名',
    'daohang/title/info_referer'        => '网址',
    'daohang/title/tag_name'            => '标签',
    'daohang/title/info_excerpt'        => '摘要',
    'daohang/title/info_content'        => '详情',
    'daohang/title/info_title'          => 'SEO标题',
    'daohang/title/info_keywords'       => 'SEO关键字',
    'daohang/title/info_description'    => 'SEO描述',
    'daohang/title/info_unique'         => '采集标识',
    'daohang/title/info_domain'         => '顶级域名',
    'daohang/title/info_status'         => '状态',
    'daohang/title/info_type'           => '属性',
    'daohang/title/info_color'          => '颜色',
    'daohang/title/info_up'             => '点赞',
    'daohang/title/info_down'           => '讨厌',
    'daohang/title/image_ico'           => '图标',
    'daohang/title/image_level'         => '封面',
    'daohang/title/info_tpl'            => '模板',
    'daohang/title/info_order'          => '权重',
    'daohang/title/info_views'          => '人气',
    'daohang/title/info_hits'           => '点击',
    //表单选项
    'daohang/option/info_type_index'    => '标准',
    'daohang/option/info_type_fast'     => '免审',
    'daohang/option/info_type_recommend'=> '推荐',
    'daohang/option/info_type_hot'      => '热门',
    'daohang/option/info_type_head'     => '头条',
    'daohang/option/info_type_foot'     => '底部',
    
    //表单提示
    'daohang/placeholder/info_name'     => '名称必须填写',
    'daohang/placeholder/info_slug'     => '拼音别名必须填写（唯一值）',
    'daohang/placeholder/info_excerpt'  => '给网站做一个简单的描述',
    'daohang/placeholder/info_content'  => '给网站做一个详细的网站介绍，支持部份HTML语法（<i>、<strong>、<p>）',
    'daohang/placeholder/info_referer'  => '待收录的网站地址，需要http(s)协议开头',
    'daohang/placeholder/image_ico'     => '可上传小图片（通常为64*64）或图片外链',
    'daohang/placeholder/image_level'   => '可上传9:16的纵向封面或图片外链',
    'daohang/placeholder/info_tpl'      => '独立模板名（不含后缀）',
    'daohang/placeholder/info_domain'   => '留空则自动分析网址的顶级域名，便于按域名筛选',
    'daohang/placeholder/info_unique'   => '采集入库时唯一验证标识',

    //前台权限
    'daohang/score/save'                 => '积分发布',
    'daohang/fast/save'                  => '免审发布',
    'daohang/search/common'              => '高级搜索',
    'daohang/filter/index'               => '高级筛选',
    'daohang/referer/index'              => '网址一键解析',
    'daohang/data/index'                 => '网址导航API',
    
    //前台筛选
    'daohang/filter/order_desc'          => '倒序',
    'daohang/filter/order_asc'           => '正序',

    //前台搜索
    'daohang/search/index'               => '网址',
    'daohang/search/baidu'               => '百度',
    'daohang/search/sogou'               => '搜狗',
    'daohang/search/toutiao'             => '头条',
    'daohang/search/bing'                => '必应',
    'daohang/search/so'                  => '360',
    
    //前台发布
    'daohang/publish/index'              => '免费发布网站',
    'daohang/fast/index'                 => '免审发布网站',

    //后台欢迎界面
    'admin/daohang/count/detail'                  => '收录网站',
    'admin/daohang/count/hidden'                  => '待审网站',
    'admin/daohang/count/category'                => '导航分类',
    'admin/daohang/count/tag'                     => '导航标签',
    'admin/daohang/count/view'                    => '导航流量',
    'admin/daohang/count/user'                    => '会员用户',
    
    //网后台站管理
    'admin/daohang/admin/index'                   => '网站管理',
    'admin/daohang/admin/create'                  => '添加网站',
    'admin/daohang/admin/edit'                    => '修改网站',
    'admin/daohang/admin/save'                    => '保存网站',
    'admin/daohang/admin/delete'                  => '删除网站',
    
    //后台采集管理
    'admin/daohang/collect/index'                 => '采集管理',
    'admin/daohang/collect/create'                => '添加采集',
    'admin/daohang/collect/edit'                  => '修改采集',
    'admin/daohang/collect/save'                  => '保存采集规则',
    'admin/daohang/collect/delete'                => '删除采集规则',
    'admin/daohang/collect/write'                 => '一键采集数据',
    'admin/daohang/collect/server'                => '资源站',
    //错误提示
    'admin/daohang/error/collect_404'             => '无法链接资源站，请检查网址是否正确或禁用了CURL函数',
    //表单标题
    'admin/daohang/title/collect_name'            => '资源站名',
    'admin/daohang/title/collect_url'             => '资源地址',
    'admin/daohang/title/collect_token'           => '授权密钥',
    'admin/daohang/title/collect_category'        => '分类转换',
    //表单提示
    'admin/daohang/placeholder/collect_url'       => 'http或https开头的网址',
    'admin/daohang/placeholder/collect_token'     => '不单独设置则使用系统全局设置的令牌',
    'admin/daohang/placeholder/collect_category'  => '一行一个，使用 => 分隔，无需转换的留空（不存在的分类会自动创建）',
    
    //后台SEO优化
    'admin/daohang/seo/index'                     => 'SEO优化',
    //表单标题
    'admin/daohang/title/rewrite_index'           => '首页伪静态',
    'admin/daohang/title/rewrite_category'        => '分类页伪静态',
    'admin/daohang/title/rewrite_tag'             => '标签页伪静态',
    'admin/daohang/title/rewrite_search'          => '搜索页伪静态',
    'admin/daohang/title/rewrite_detail'          => '详情页伪静态',
    'admin/daohang/title/rewrite_filter'          => '筛选页伪静态',
    'admin/daohang/title/index_title'             => '首页－标题',
    'admin/daohang/title/index_keywords'          => '首页－关键字',
    'admin/daohang/title/index_description'       => '首页－描述',
    'admin/daohang/title/search_title'            => '搜索页－标题',
    'admin/daohang/title/search_keywords'         => '搜索页－关键字',
    'admin/daohang/title/search_description'      => '搜索页－描述',
    'admin/daohang/title/category_title'          => '所有分类页－标题',
    'admin/daohang/title/category_keywords'       => '所有分类页－关键字',
    'admin/daohang/title/category_description'    => '所有分类页－描述',
    'admin/daohang/title/tag_title'               => '所有标签页－标题',
    'admin/daohang/title/tag_keywords'            => '所有标签页－关键字',
    'admin/daohang/title/tag_description'         => '所有标签页－描述',
    'admin/daohang/title/publish_title'           => '免费收录页－标题',
    'admin/daohang/title/publish_keywords'        => '免费收录页－关键字',
    'admin/daohang/title/publish_description'     => '免费收录页－描述',
    'admin/daohang/title/fast_title'              => '付费免审页－标题',
    'admin/daohang/title/fast_keywords'           => '付费免审页－关键字',
    'admin/daohang/title/fast_description'        => '付费免审页－描述',
    //表单提示
    'admin/daohang/placeholder/rewrite_index'      => '需要将此应用设为全站首页，则填写/',
    
    //后台频道设置
    'admin/daohang/config/index'                  => '频道设置',
    'admin/daohang/title/theme'                   => '电脑端模板',
    'admin/daohang/title/theme_wap'               => '移动端模板',
    'admin/daohang/title/slug_first'              => '别名首字母',
    'admin/daohang/title/jump_page'               => '跳转广告页',
    'admin/daohang/title/publish_save'            => '免费发布开关',
    'admin/daohang/title/value_login'             => '顶踩需要登录',
    'admin/daohang/title/score_fast'              => '快审扣除积分',
    'admin/daohang/title/limit_index_web'         => '首页网站数量',
    'admin/daohang/title/limit_index_category'    => '首页栏目数量',
    'admin/daohang/title/limit_index_tag'         => '首页标签数量',
    'admin/daohang/title/limit_index_hot'         => '首页热门数量',
    'admin/daohang/title/limit_category'          => '分类页每页数量',
    'admin/daohang/title/limit_tag'               => '标签页每页数量',
    'admin/daohang/title/limit_sitemap'           => '地图页分页数量',
    'admin/daohang/title/limit_search'            => '搜索页分页数量',
    'admin/daohang/title/limit_tag_input'         => '标签表单展示数',
    'admin/daohang/title/request_max'             => '最大请求次数',
    'admin/daohang/title/search_list'             => '搜索引擎列表',
    'admin/daohang/title/search_interval'         => '搜索间隔频率',
    'admin/daohang/title/search_hot'              => '热门关键词',
    'admin/daohang/title/label_strip'             => '允许HTML标签',
    'admin/daohang/title/post_pwd'                => '采集器入库密码',
    'admin/daohang/title/plus_action'             => '内容模型扩展',
    'admin/daohang/title/type_option'             => '内容属性扩展',
    //表单提示
    'admin/daohang/placeholder/theme'             => '请选择默认主题目录名',    
    'admin/daohang/placeholder/theme_wap'         => '请选择移动端独立主题目录名',
    'admin/daohang/placeholder/slug_first'        => '开启时为首字母，关闭时为全拼',
    'admin/daohang/placeholder/jump_page'         => '普通网址跳转时是否显示中间广告页、关闭后直接302跳转',
    'admin/daohang/placeholder/publish_save'      => '关闭后将禁用免费发布网站功能',
    'admin/daohang/placeholder/value_login'       => '开启后必需登录才能参与互动',
    'admin/daohang/placeholder/score_fast'        => '发布一条免审核网站扣除的积分数量，设为0将不启用积分发布功能',
    'admin/daohang/placeholder/limit_index'       => '设为0不启用、小于100',
    'admin/daohang/placeholder/limit_category'    => '设为0不启用',
    'admin/daohang/placeholder/limit_tag'         => '设为0不启用',
    'admin/daohang/placeholder/limit_sitemap'     => '限制Sitemap地图页每页数量',
    'admin/daohang/placeholder/limit_search'      => '限制搜索页每页数量',
    'admin/daohang/placeholder/request_max'       => '5分钟内最大请求次数，设为0不启用',
    'admin/daohang/placeholder/search_list'       => '引导到第三方搜索引擎的列表，逗号分隔',
    'admin/daohang/placeholder/search_interval'   => '规定时间内（秒）只能搜索一次，设为0不启用',
    'admin/daohang/placeholder/search_hot'        => '自定义热门搜索关键词，以逗号分隔',
    'admin/daohang/placeholder/label_strip'       => 'daohangStrip函数允许的HTML标签',
    'admin/daohang/placeholder/post_pwd'          => '采集器入库时必须添加的密码字段（post_pwd）,留空则不启用此功能',
    'admin/daohang/placeholder/plus_action'       => '扩展网站内容模型（操作名），多个用逗号分隔',
    'admin/daohang/placeholder/type_option'       => '扩展网站内容属性、json格式',
];