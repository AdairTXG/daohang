{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("user/admin/reward")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
	{:lang("user/admin/reward")}
</h6>
{:DcBuildForm([
    'name'     => 'user/reward/index',
    'class'    => 'bg-white py-2',
    'action'   => DcUrlAddon(['module'=>'user','controll'=>'reward','action'=>'update']),
    'method'   => 'post',
    'ajax'     => true,
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'disabled' => false,
    'callback' => '',
    'items'    => $items,
    'class_button' => 'form-group text-left mb-0',
])}
{/block}