<?php
namespace app\daohang\loglic;

class Seo
{
    public function fields($data=[])
    {
        $items = [
            'rewrite_index' => [
                'type'        => 'text', 
                'value'       => DcEmpty(config('daohang.rewrite_index'), 'daohang$'),
                'placeholder' => lang('admin/daohang/placeholder/rewrite_index'),
                'tips'        => 'daohang$',
            ],
            'rewrite_detail' => [
                'type'        => 'text',
                'value'       => config('daohang.rewrite_detail'),
                'placeholder' => '',
                'tips'        => '[:id] [:slug] [:name] [:termId] [:termSlug] [:termName]',
            ],
            'rewrite_category' => [
                'type'        => 'text', 
                'value'       => config('daohang.rewrite_category'),
                'placeholder' => '',
                'tips'        => '[:id] [:slug] [:name] [:pageNumber]',
            ],
            'rewrite_tag' => [
                'type'        => 'text',
                'value'       => config('daohang.rewrite_tag'),
                'placeholder' => '',
                'tips'        => '[:id] [:slug] [:name] [:pageNumber]',
            ],
            'rewrite_search' => [
                'type'        => 'text',
                'value'       => config('daohang.rewrite_search'),
                'placeholder' => '',
                'tips'        => '[:searchText] [:pageNumber] [:pageSize]',
            ],
            'rewrite_filter' => [
                'type'        => 'text',
                'value'       => config('daohang.rewrite_filter'),
                'placeholder' => '',
                'tips'        => '[:termId] [:termSlug] [:termName] [:pageNumber] [:pageSize] [:sortName] [:sortNumber]',
            ],
            'html_hr' => [
                'type'        => 'html',
                'value'       => '<hr>',
            ],
            'index_title'                => ['type'=>'text', 'value'=>config('daohang.index_title')],
            'index_keywords'             => ['type'=>'text', 'value'=>config('daohang.index_keywords')],
            'index_description'          => ['type'=>'text', 'value'=>config('daohang.index_description')],
            'html_hr2'                   => ['type'=> 'html','value'=>'<hr>'],
            'search_title'               => ['type'=>'text', 'value'=>config('daohang.search_title')],
            'search_keywords'            => ['type'=>'text', 'value'=>config('daohang.search_keywords')],
            'search_description'         => ['type'=>'text', 'value'=>config('daohang.search_description')],
            'html_hr3'                   => ['type'=> 'html','value'=>'<hr>'],
            'category_title'             => ['type'=>'text', 'value'=>config('daohang.category_title')],
            'category_keywords'          => ['type'=>'text', 'value'=>config('daohang.category_keywords')],
            'category_description'       => ['type'=>'text', 'value'=>config('daohang.category_description')],
            'html_hr4'                   => ['type'=> 'html','value'=>'<hr>'],
            'tag_title'                  => ['type'=>'text', 'value'=>config('daohang.tag_title')],
            'tag_keywords'               => ['type'=>'text', 'value'=>config('daohang.tag_keywords')],
            'tag_description'            => ['type'=>'text', 'value'=>config('daohang.tag_description')],
            'html_hr5'                   => ['type'=> 'html','value'=>'<hr>'],
            'publish_title'              => ['type'=>'text', 'value'=>config('daohang.publish_title')],
            'publish_keywords'           => ['type'=>'text', 'value'=>config('daohang.publish_keywords')],
            'publish_description'        => ['type'=>'text', 'value'=>config('daohang.publish_description')],
            'html_hr6'                   => ['type'=> 'html','value'=>'<hr>'],
            'fast_title'                 => ['type'=>'text', 'value'=>config('daohang.fast_title')],
            'fast_keywords'              => ['type'=>'text', 'value'=>config('daohang.fast_keywords')],
            'fast_description'           => ['type'=>'text', 'value'=>config('daohang.fast_description')],
        ];
        
        foreach($items as $key=>$value){
            $items[$key]['title']  = lang('admin/daohang/title/'.$key);
        }
        
        return $items;
    }
}