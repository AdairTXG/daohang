{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:daohangSeo(lang('daohang/fast/'.$action.'/title'))}－{:config('common.site_name')}</title>
<meta name="keywords" content="{:daohangSeo(lang('daohang/fast/'.$action.'/keywords'))}" />
<meta name="description" content="{:daohangSeo(lang('daohang/fast/'.$action.'/description'))}" />
{/block}
<!-- -->
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
<div class="rounded bg-white pt-4 px-3 pb-3">
<!---->
<h2 class="text-center mb-3">免审发布</h2>
<h6 class="text-center small mb-3">免费发布的信息需要管理员审核通过后才能显示、如需免审核服务请点击“<a class="text-purple" href="{:DcUrl('daohang/publish/'.$action)}">这里</a>”。</h6>
{if $user['user_id'] lt "1"}
  <div class="alert alert-danger mb-3">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>免审发布权限</strong> 系统检测到您还没有登录，请先<a class="text-purple mx-1" href="{:daohangUrl('user/register/index')}">注册</a>或<a class="text-purple mx-1" href="{:daohangUrl('user/login/index')}">登录</a>
  </div>
  {else /}
  <div class="alert alert-secondary mb-3">
    <p class="mb-0"><strong>按等级发布：</strong>一次性升级到VIP用户组、发布网站不限数量。 {if in_array('vip',$user['user_capabilities'])}<i class="fa fa-check-circle text-success"></i>{else/}<a class="text-purple" href="{:DcUrl('user/group/index')}">我要升级</a>{/if}</p>
    {if $scoreFast}
    <p class="mt-2 mb-0"><strong>按积分发布：</strong>每发布一条信息扣除（{$scoreFast}）积分、当前积分（{$user.user_score|intval}）个。<a class="text-purple" href="{:DcUrl('user/recharge/index')}">我要充值</a><small class="text-muted">（1元人民币等于{$scoreRecharge}个积分）</small></p>
    {/if}
  </div>
{/if}
<section class="border rounded p-3">
{:DcBuildForm([
    'name'     => 'daohang/fast/'.$action,
    'class'    => 'w-100',
    'action'   => daohangUrl('daohang/fast/save'),
    'method'   => 'post',
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'ajax'     => true,
    'disabled' => false,
    'callback' => 'daicuo.daohang.dialog',
    'data'     => false,
    'items'    => $fields,
    'submit_class' => 'btn btn-purple',
    'reset_class'  => 'btn btn-dark',
])}
</section>
</div>
</div>
{/block}
{block name="footer"}{include file="widget/footer" /}{/block}