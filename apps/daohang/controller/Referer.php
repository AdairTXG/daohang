<?php
namespace app\daohang\controller;

use app\common\controller\Api;

class Referer extends Api
{
    protected $auth = [
         'check'       => true,
         'rule'        => 'daohang/referer/index',
         'none_login'  => [],
         'none_right'  => [],
         'error_login' => 'user/login/index',
         'error_right' => 'user/group/index',
    ];
    
    public function _initialize()
    {
		parent::_initialize();
    }
    
    public function index()
    {
        $result = model('daohang/Info','loglic')->curl(input('url/s'));
        if($result){
            $this->success(lang('success'), $result);
        }else{
            $this->error(lang('daohang/error/referer'));
        }
    }
}