<?php
namespace app\daohang\controller;

use app\common\controller\Api;

class Json extends Api
{
    protected $auth = [
         'check'       => false,
         'none_login'  => ['daohang/json/hits'],
         'none_right'  => [],
         'error_login' => 'daohang/json/index',
         'error_right' => '',
    ];
    
    public function _initialize()
    {
		parent::_initialize();
    }
    
    public function index()
    {
        $this->error(lang('empty'), ['value'=>0]);
    }
}