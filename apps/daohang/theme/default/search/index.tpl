{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle|DcEmpty='呆错导航系统'}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords|DcEmpty='呆错导航系统,daiduodaohang'}" />
<meta name="description" content="{$seoDescription|DcEmpty='呆错导航系统是一款免费开源的专业分类导航建站系统。'}"  />
{/block}
<!-- -->
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container pt-2">
<!---->
<div class="row dh-row">
  <div class="col-12 col-md-3 px-1 order-2 order-md-1">
    <div class="card mb-2">
      <div class="card-header px-2">
        <i class="fa fa-fw fa-fire mr-1 text-purple"></i>热门关键词
      </div>
      <div class="card-body px-2 pb-0">
        {volist name=":explode(',',config('daohang.search_hot'))" id="keyword"}
        <p class="text-truncate">
          {gt name="i" value="3"}
          <span class="badge dh-badge badge-secondary mr-2">{$i}</span>
          {else/}
          <span class="badge dh-badge badge-purple mr-2">{$i}</span>
          {/gt}
          <a class="text-dh text-dark" href="{:url('daohang/search/index',['searchText'=>$keyword],'')}">{$keyword}</a>
          <small class="float-right text-muted">{:date('m-d',time())}</small>
        </p>
        {/volist}
      </div> 
    </div>
    {include file="widget/ads250" /}
  </div>
  <!---->
  <div class="col-12 col-md-9 px-1 order-1 order-md-2">
    <div class="card mb-2 px-3 pt-3">
      {if $searchList}
      <ul class="nav nav-pills border-bottom pb-2 mb-3">
        {volist name="searchList" id="searchUrl"}
        {if $key eq 'daohang/search/index'}
        <li class="nav-item"><a class="nav-link active" href="{$searchUrl}" target="{:daohangSearchTarget($key)}">{:lang($key)}</a></li>
        {else/}
        <li class="nav-item"><a class="nav-link" href="{$searchUrl}" target="{:daohangSearchTarget($key)}">{:lang($key)}</a></li>
        {/if}
        {/volist}
      </ul>
      {/if}
      <div class="mb-3 text-muted">
        {:config('common.site_name')}为您找到相关结果约<strong class="mx-1 text-purple">{$total|default=0}</strong>个
      </div>
      {foreach data as $daohang}
      <h6 class="text-truncate">
        <a class="font-weight-bold {$daohang.info_color|daohangColor}" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|DcHtml}</a>
      </h6>
      <h6 class="text-muted small">
        {$daohang.info_referer|daohangReferer}
      </h6>
      <p class="border-bottom pb-2 mb-4">
        {$daohang.info_excerpt|daohangSubstr=0,86,true}
      </p>
      {/foreach}
      <!---->
      {gt name="last_page" value="1"}
      <div class="rounded bg-white pt-3 mb-2 d-md-none d-flex justify-content-center">{:DcPageSimple($current_page, $last_page, $pagePath)}</div>
      <div class="rounded bg-white pt-3 mb-2 d-none d-md-flex justify-content-md-center">{:DcPage($current_page, $per_page, $total, $pagePath)}</div>
      {/gt}
    </div>
  </div>
  <!---->
</div>
</div>
{/block}
<!-- -->
{block name="footer"}{include file="widget/footer" /}{/block}