<?php
namespace app\user\loglic;

use app\common\loglic\Update;

class Upgrade extends Update
{
    //在线升级回调
    public function init()
    {
        return true;
    }
    
    //更新应用状态
    public function status()
    {
        \daicuo\Apply::updateStatus('user', 'enable');
        
        return true;
    }
    
    //更新应用打包信息
    public function pack()
    {
        return true;
    }
    
    //更新应用配置
    public function config()
    {
        return true;
    }
    
    //更新前台菜单
    public function navs()
    {
        return true;
    }
    
    //修改后台菜单
    public function menu()
    {
        return true;
    }
}