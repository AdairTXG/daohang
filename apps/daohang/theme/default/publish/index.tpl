{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle|DcEmpty='呆错导航系统免费收录网站'}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords|DcEmpty='呆错导航系统,daiduodaohang'}" />
<meta name="description" content="{$seoDescription|DcEmpty='呆错导航系统是一款免费开源的专业分类导航建站系统。'}"  />
{/block}
<!-- -->
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
<div class="rounded bg-white pt-4 px-3 pb-3">
<!---->
<h2 class="text-center mb-3">免费发布网站</h2>
<h6 class="text-center small mb-3">免费提交的网站需要管理员审核通过后才能显示、如需快速免审核服务请点击“<a class="text-purple" href="{:daohangUrl('daohang/fast/index')}">这里</a>”。</h6>
<div class="alert alert-secondary mb-3">
  {$seoDescription|DcEmpty='呆错导航系统是一款免费开源的专业分类导航建站系统，不收录任何非法或灰色边缘网站、谢谢合作！'}
</div>
<section class="border rounded p-3">
<div class="row">
  {:DcBuildForm([
    'name'     => 'daohang/publish/index',
    'class'    => 'col-12 col-md-9 order-2',
    'action'   => daohangUrl('daohang/publish/save'),
    'method'   => 'post',
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'ajax'     => true,
    'disabled' => false,
    'callback' => false,
    'data'     => false,
    'items'    => $fields,
    'submit_class' => 'btn btn-purple',
    'reset_class'  => 'btn btn-dark',
  ])}
  <div class="col-12 col-md-3 order-1">
    <section class="mb-4 text-muted">
      <h6 class="mb-3 text-dark">{:config('common.site_name')}收录条款</h6>
      <p class="small">1、只收录拥有顶级域名的网站；</p>
      <p class="small">2、只收录页面制作精良的网站；</p>
      <p class="small">3、务必在当天做好友情连接；</p>
      <p class="small">4、不收录非法或灰色边缘网站；</p>
      <p class="small">5、不收录没做好友情连接的网站；</p>
    </section>
    <section class="mb-0 text-muted">   
       <h6 class="mb-3 text-dark">{:config('common.site_name')}链接信息</h6>
      <p class="small">网站名称：{:config('common.site_name')}</p>
      <p class="small">网站地址：{$domain}</p>
    </section>
  </div>
</div>
</section>
<!---->
</div>
</div>
{/block}
{block name="footer"}{include file="widget/footer" /}{/block}