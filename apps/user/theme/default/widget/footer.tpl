<div class="fixed-bottom">
  <div class="container py-2 text-md-center">
    Copyright © 2019-2022 {:config('common.site_domain')} All rights reserved
    {if config('common.site_icp')}<a class="text-white" href="https://beian.miit.gov.cn" target="_blank">{:config('common.site_icp')}</a>{/if}
  </div>
</div>