<?php
return [
    'admin_index_count' => [
        'app\\daohang\\behavior\\Hook',
        '_overlay' => true,
    ],
    'admin_index_footer' => [
        'app\\daohang\\behavior\\Hook',
    ],
    'admin_caps_front' => [
        'app\\daohang\\behavior\\Hook',
    ],
    'admin_caps_back' => [
        'app\\daohang\\behavior\\Hook',
    ]
];