{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords}" />
<meta name="description" content="{$seoDescription}" />
{/block}
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
{if $limitWeb}
<div class="row dh-row">
  <div class="col-12 px-1 mb-2">
    <div class="card">
      <div class="card-header px-2 d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-desktop mr-1 text-purple"></i><a class="text-dark" href="{:DcUrl('daohang/category/index',['slug'=>'wzdq'])}">网站大全</a></span>
        <a class="text-purple small" href="{:daohangUrl('daohang/publish/index')}">{:lang('daohang/publish/index')}</a>
      </div>
      <div class="card-body row pb-0">
        {volist name=":daohangSelect(['field'=>'info_id,info_name,info_slug,info_type,info_action','with'=>'term,info_meta','type'=>['in','head,fast,recommend'],'status'=>'normal','limit'=>$limitWeb,'sort'=>'info_order desc,info_update_time','order'=>'desc'])" id="web" mod="9"}
        <p class="col-6 col-md-3 col-lg-2 text-truncate">
          <a class="{$web.info_color|daohangColor}" href="{:daohangUrlJump($web['info_type'],$web['info_referer'],$web['info_id'])}" target="_blank" data-id="{$web.info_id}" data-type="{$web.info_type|default='index'}">{if $web['info_type'] eq 'fast'}<i class="mr-1 fa fa-fw fa-clone text-purple"></i>{else/}<i class="mr-1 fa fa-fw fa-clone mr-1"></i>{/if}{$web.info_name|daohangSubstr=0,10}</a>
        </p>
        {/volist}
      </div>
    </div>
  </div>
</div>
<!---->
{include file="widget/ads970" /}
{/if}
<div class="row dh-row">
  <div class="col-12 col-md-4 col-lg-3 px-1 order-2 order-lg-1">
    {if $limitHot}
    <div class="card mb-2">
      <div class="card-header px-2 d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-fire mr-1 text-purple"></i>热门网站</span>
        <a class="small" href="{:DcUrl('daohang/page/views')}">更多>></a>
      </div>
      <div class="card-body px-2 pb-0">
        {volist name=":daohangSelect(['field'=>'info_id,info_name,info_slug,info_type,info_action,info_views','with'=>'term','status'=>'normal','limit'=>$limitHot,'sort'=>'info_views','order'=>'desc'])" id="daohang"}
        <p class="text-truncate">
          {gt name="i" value="3"}
          <span class="badge dh-badge badge-secondary mr-2">{$i}</span>
          {else/}
          <span class="badge dh-badge badge-purple mr-2">{$i}</span>
          {/gt}
          <a class="{$daohang.info_color|daohangColor}" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|daohangSubstr=0,12,false}</a>
          <small class="float-right text-muted">{$daohang.info_views|number_format}</small>
        </p>
        {/volist}
      </div> 
    </div>
    {/if}
    {include file="widget/ads250" /}
  </div>
  <!---->
  <div class="col-12 col-md-8 col-lg-9 px-1 order-1 order-lg-2">
    <div class="card mb-2">
      <div class="card-header px-2 d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-navicon mr-1 text-purple"></i>栏目分类</span>
        <a class="small" href="{:DcUrl('daohang/page/types')}" target="_self">全部>></a>
      </div>
      <div class="card-body pb-0 row dh-row">
        {volist name=":daohangCategorySelect(['field'=>'term_id,term_name,term_slug,term_action','action'=>['eq','index'],'status'=>'normal','limit'=>$limitCategory,'sort'=>'term_order','order'=>'desc'])" id="category"}
        {if $items=daohangSelect(['field'=>'info_id,info_name,info_slug,info_type,info_action','with'=>'term','term_id'=>['eq',$category['term_id']],'status'=>'normal','limit'=>7,'sort'=>'info_id','order'=>'desc'])}
        <p class="col-4 col-md-3 col-lg-12 px-1 text-truncate d-lg-flex flex-row justify-content-between align-items-center">
          <a class="btn btn-light btn-sm btn-sm-block" href="{:daohangUrlCategory($category)}" target="_self">{$category.term_name}</a>
          {volist name="$items" id="daohang"}
          <a class="d-none d-lg-inline {$daohang.info_color|daohangColor}" href="{:daohangUrlInfo($daohang)}" target="_self">{$daohang.info_name|daohangSubstr=0,6,false}</a>
          {/volist}
          <a class="small text-muted d-none d-lg-inline" href="{:daohangUrlCategory($category)}" target="_self">更多>></a>
        </p>
        {/if}
        {/volist}
      </div> 
    </div>
    {include file="widget/ads728" /}
    {if $limitTag}
    <div class="card mb-2">
      <div class="card-header px-2 d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-tags mr-1 text-purple"></i>网站标签</span>
        <a class="small" href="{:DcUrl('daohang/page/tags')}">更多>></a>
      </div>
      <div class="card-body px-2 pb-0 text-center row dh-row">
        {volist name=":daohangTagSelect(['field'=>'term_id,term_name,term_slug,term_action','status'=>'normal','limit'=>$limitTag,'sort'=>'term_count','order'=>'desc'])" id="tag"}
        <p class="col-4 col-md-3 col-lg-2">
          <a class="btn btn-light btn-block btn-sm" href="{:daohangUrlTag($tag)}">{$tag.term_name|daohangSubstr=0,4,false}</a>
        </p>
        {/volist}
      </div>
    </div>
    {/if}
  </div>
</div>
    
</div>
<!---->
{include file="widget/friend" /}
{/block}
<!-- -->
{block name="footer"}{include file="widget/footer" /}{/block}