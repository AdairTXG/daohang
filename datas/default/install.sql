DROP TABLE IF EXISTS `dc_info`;
CREATE TABLE `dc_info` (
  `info_id` bigint(20) NOT NULL,
  `info_name` varchar(255) DEFAULT NULL,
  `info_slug` varchar(255) DEFAULT NULL,
  `info_excerpt` text,
  `info_content` longtext,
  `info_password` varchar(255) DEFAULT NULL,
  `info_create_time` int(11) DEFAULT '0',
  `info_update_time` int(11) DEFAULT '0',
  `info_parent` bigint(20) DEFAULT '0',
  `info_order` int(11) DEFAULT '0',
  `info_user_id` bigint(20) DEFAULT '0',
  `info_type` varchar(80) DEFAULT NULL,
  `info_mime_type` varchar(100) DEFAULT NULL,
  `info_status` varchar(60) DEFAULT 'normal',
  `info_comment_status` varchar(100) DEFAULT 'open',
  `info_comment_count` bigint(20) DEFAULT '0',
  `info_views` bigint(20) DEFAULT '0',
  `info_hits` bigint(20) DEFAULT '0',
  `info_module` varchar(80) DEFAULT 'common',
  `info_controll` varchar(80) DEFAULT NULL,
  `info_action` varchar(80) DEFAULT NULL,
  `info_title` varchar(255) DEFAULT NULL,
  `info_keywords` varchar(255) DEFAULT NULL,
  `info_description` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dc_info_meta`;
CREATE TABLE `dc_info_meta` (
  `info_meta_id` bigint(20) NOT NULL,
  `info_meta_key` varchar(255) NOT NULL,
  `info_meta_value` longtext,
  `info_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dc_log`;
CREATE TABLE `dc_log` (
  `log_id` bigint(20) NOT NULL,
  `log_user_id` bigint(20) NOT NULL DEFAULT '0',
  `log_info_id` bigint(20) NOT NULL DEFAULT '0',
  `log_value` int(11) NOT NULL DEFAULT '0' COMMENT '整数值',
  `log_decimal` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT '小数值',
  `log_status` varchar(60) NOT NULL DEFAULT 'normal',
  `log_module` varchar(150) DEFAULT NULL,
  `log_controll` varchar(150) DEFAULT NULL,
  `log_action` varchar(150) DEFAULT NULL,
  `log_type` varchar(100) DEFAULT NULL,
  `log_ip` varchar(250) DEFAULT NULL,
  `log_name` varchar(250) DEFAULT NULL,
  `log_info` tinytext,
  `log_create_time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dc_op`;
CREATE TABLE `dc_op` (
  `op_id` bigint(20) NOT NULL,
  `op_name` varchar(150) NOT NULL,
  `op_value` longtext,
  `op_module` varchar(50) DEFAULT NULL,
  `op_controll` varchar(50) DEFAULT NULL,
  `op_action` varchar(50) DEFAULT NULL,
  `op_order` int(11) DEFAULT '0',
  `op_autoload` varchar(20) DEFAULT 'yes',
  `op_status` varchar(60) DEFAULT 'normal'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dc_term`;
CREATE TABLE `dc_term` (
  `term_id` bigint(20) NOT NULL,
  `term_name` varchar(255) DEFAULT NULL,
  `term_slug` varchar(255) DEFAULT NULL,
  `term_module` varchar(100) DEFAULT 'common',
  `term_controll` varchar(100) DEFAULT 'common',
  `term_action` varchar(100) DEFAULT 'common',
  `term_status` varchar(60) DEFAULT 'normal',
  `term_order` int(11) DEFAULT '0',
  `term_type` varchar(100) DEFAULT NULL,
  `term_info` text,
  `term_parent` bigint(20) NOT NULL DEFAULT '0',
  `term_count` int(11) NOT NULL DEFAULT '0',
  `term_title` varchar(255) DEFAULT NULL,
  `term_keywords` varchar(255) DEFAULT NULL,
  `term_description` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dc_term_map`;
CREATE TABLE `dc_term_map` (
  `detail_id` bigint(20) DEFAULT '0',
  `term_id` bigint(20) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dc_term_meta`;
CREATE TABLE `dc_term_meta` (
  `term_meta_id` bigint(20) NOT NULL,
  `term_meta_key` varchar(255) NOT NULL,
  `term_meta_value` longtext,
  `term_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dc_user`;
CREATE TABLE `dc_user` (
  `user_id` bigint(20) NOT NULL,
  `user_email` varchar(150) DEFAULT NULL,
  `user_name` varchar(150) NOT NULL,
  `user_nice_name` varchar(255) DEFAULT NULL,
  `user_mobile` varchar(20) DEFAULT NULL,
  `user_pass` varchar(32) NOT NULL,
  `user_status` varchar(60) NOT NULL DEFAULT 'normal',
  `user_create_time` int(11) DEFAULT '0',
  `user_update_time` int(11) DEFAULT '0',
  `user_create_ip` varchar(32) DEFAULT NULL,
  `user_update_ip` varchar(32) DEFAULT NULL,
  `user_slug` varchar(255) DEFAULT NULL,
  `user_views` bigint(20) DEFAULT '0',
  `user_hits` bigint(20) DEFAULT '0',
  `user_token` varchar(255) DEFAULT NULL,
  `user_module` varchar(100) DEFAULT 'common',
  `user_controll` varchar(100) DEFAULT NULL,
  `user_action` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `dc_user` (`user_id`, `user_email`, `user_name`, `user_nice_name`, `user_mobile`, `user_pass`, `user_status`, `user_create_time`, `user_update_time`, `user_create_ip`, `user_update_ip`, `user_slug`, `user_views`, `user_hits`, `user_token`, `user_module`, `user_controll`, `user_action`) VALUES
(1, 'admin@daicuo.org', 'admin', '', '13800138000', '7fef6171469e80d32c0559f88b377245', 'normal', 0, 1599138991, '0', '127.0.0.1', '', 0, 0, NULL, 'common', '', '');

DROP TABLE IF EXISTS `dc_user_meta`;
CREATE TABLE `dc_user_meta` (
  `user_meta_id` bigint(20) NOT NULL,
  `user_meta_key` varchar(255) NOT NULL,
  `user_meta_value` longtext,
  `user_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `dc_user_meta` (`user_meta_id`, `user_meta_key`, `user_meta_value`, `user_id`) VALUES
(20, 'user_capabilities', 'a:2:{i:0;s:5:\"guest\";i:1;s:13:\"administrator\";}', 1);


ALTER TABLE `dc_info`
  ADD PRIMARY KEY (`info_id`),
  ADD KEY `info_slug` (`info_slug`),
  ADD KEY `info_create_time` (`info_create_time`),
  ADD KEY `info_update_time` (`info_update_time`),
  ADD KEY `info_order` (`info_order`),
  ADD KEY `info_views` (`info_views`),
  ADD KEY `info_hits` (`info_hits`),
  ADD KEY `info_parent` (`info_parent`),
  ADD KEY `info_user_id` (`info_user_id`),
  ADD INDEX `info_count` (`info_module`, `info_controll`, `info_action`, `info_status`);

ALTER TABLE `dc_info_meta`
  ADD PRIMARY KEY (`info_meta_id`),
  ADD INDEX `info_meta` (`info_id`, `info_meta_key`);
  
ALTER TABLE `dc_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `log_user_id` (`log_user_id`),
  ADD KEY `log_info_id` (`log_info_id`),
  ADD KEY `log_status` (`log_status`),
  ADD KEY `log_module` (`log_module`),
  ADD KEY `log_controll` (`log_controll`),
  ADD KEY `log_action` (`log_action`),
  ADD KEY `log_type` (`log_type`);

ALTER TABLE `dc_op`
  ADD PRIMARY KEY (`op_id`),
  ADD KEY `op_module` (`op_module`),
  ADD KEY `op_status` (`op_status`);

ALTER TABLE `dc_term`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `term_name` (`term_name`),
  ADD KEY `term_slug` (`term_slug`),
  ADD KEY `term_status` (`term_status`),
  ADD KEY `term_type` (`term_type`),
  ADD KEY `term_parent` (`term_parent`),
  ADD KEY `term_module` (`term_module`),
  ADD KEY `term_controll` (`term_controll`),
  ADD KEY `term_action` (`term_action`);

ALTER TABLE `dc_term_map`
  ADD PRIMARY KEY (`detail_id`, `term_id`);

ALTER TABLE `dc_term_meta`
  ADD PRIMARY KEY (`term_meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `term_meta_key` (`term_meta_key`);

ALTER TABLE `dc_user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_name` (`user_name`),
  ADD KEY `user_email` (`user_email`),
  ADD KEY `user_status` (`user_status`);

ALTER TABLE `dc_user_meta`
  ADD PRIMARY KEY (`user_meta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_meta_key` (`user_meta_key`);

ALTER TABLE `dc_info`
  MODIFY `info_id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `dc_info_meta`
  MODIFY `info_meta_id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `dc_log`
  MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `dc_op`
  MODIFY `op_id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `dc_term`
  MODIFY `term_id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `dc_term_meta`
  MODIFY `term_meta_id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `dc_user`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `dc_user_meta`
  MODIFY `user_meta_id` bigint(20) NOT NULL AUTO_INCREMENT;