{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>热门网站－{:config('common.site_name')}</title>
<meta name="keywords" content="网站排行榜,人气网站,浏览次数最多的网站" />
<meta name="description" content="{:config('common.site_name')}收录的网站按浏览次数人气排行榜。" />
{/block}
<!-- -->
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
{include file="widget/ads970" /}
<div class="row dh-row">
{volist name=":daohangCategorySelect([
    'result' => 'array',
    'action' => 'index',
    'status' => 'normal',
    'field'  => 'term_id,term_name,term_slug,term_slug',
    'with'   => false,
    'sort'   => 'term_order',
    'order'  => 'desc',
])" id="category"}
{if $items=daohangSelect([
    'term_id' => ['eq',$category['term_id']],
    'field'   => 'info_id,info_name,info_slug,info_type,info_action,info_views,info_create_time',
    'action'  => 'index',
    'with'    => 'term',
    'status'  => 'normal',
    'limit'   => '10',
    'sort'    => 'info_views',
    'order'   => 'desc',
])}
<div class="col-12 col-md-3 px-1 mb-2">
<div class="card">
  <div class="card-header px-2 d-flex flex-row justify-content-between align-items-center">
    <span><i class="fa fa-fw fa-desktop mr-1 text-purple"></i>{$category.term_name}</span>
    <a class="small text-purple" href="{:daohangUrlFilter('daohang/filter/index',['termId'=>$category['term_id'],'pageSize'=>30,'pageNumber'=>1,'sortName'=>'info_views','sortOrder'=>'desc'])}">更多>></a>
  </div>
  <div class="card-body pb-0 px-2">
    {volist name=":daohangSelect(['field'=>'info_id,info_name,info_slug,info_type,info_action,info_views','with'=>'term','status'=>'normal','limit'=>'10','sort'=>'info_views','order'=>'desc','term_id'=>['eq',$category['term_id']]])" id="daohang"}
    <p class="text-truncate">
      {gt name="i" value="3"}
      <span class="badge dh-badge badge-secondary mr-2">{$i}</span>
      {else/}
      <span class="badge dh-badge badge-purple mr-2">{$i}</span>
      {/gt}
      <a class="{$daohang.info_color|daohangColor}" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|daohangSubstr=0,11,false}</a>
      <small class="float-right text-muted">{$daohang.info_views|number_format}</small>
    </p>
    {/volist}
  </div> 
</div>
</div>
{/if}
{/volist}
</div>
<!---->
</div>
{/block}
{block name="footer"}{include file="widget/footer" /}{/block}
<!-- -->