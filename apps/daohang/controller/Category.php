<?php
namespace app\daohang\controller;

use app\daohang\controller\Front;

class Category extends Front
{
    public function _initialize()
    {
        $this->requestCheck();
        
        parent::_initialize();
    }
    
    //首页
    public function index()
    {
        return $this->detail();
    }
    
    //空操作
    public function _empty($action='')
    {
        //模型验证
        $this->actionCheck($action);

        //查询数据
        return $this->detail();
        
        //$this->redirect('daohang/category/index', $this->query, 302);
    }
    
    //公共操作
    private function detail()
    {
        if( isset($this->query['id']) ){
            $term = daohangCategoryId($this->query['id']);
        }elseif( isset($this->query['slug']) ){
            $term = daohangCategorySlug($this->query['slug']);
        }elseif( isset($this->query['name']) ){
            $term = daohangCategoryName($this->query['name']);
        }else{
            $this->error(lang('mustIn'),'daohang/index/index');
        }
        //数据为空
        if(!$term){
            $this->error(lang('empty'),'daohang/index/index');
        }
        //SEO标签
        $term['seoTitle']       = daohangSeo(DcEmpty($term['term_title'],$term['term_name']),$this->site['page']);
        $term['seoKeywords']    = daohangSeo(DcEmpty($term['term_keywords'],$term['term_name']),$this->site['page']);
        $term['seoDescription'] = daohangSeo(DcEmpty($term['term_description'],$term['term_name']),$this->site['page']);
        //子分类
        $term['term_ids']   = DcTermSubIds($term['term_id'],'category','array');
        //分页路径
        $term['pagePath']   = daohangUrlCategory($term,'[PAGE]');
        //地址栏
        $term['pageSize']   = intval(config('daohang.limit_category'));
        $term['pageNumber'] = $this->site['page'];
        $term['sortName']   = 'info_order desc,info_update_time';
        $term['sortOrder']  = 'desc';
        //分页数量
        if(is_numeric($term['term_limit'])){
            $term['pageSize'] = $term['term_limit'];
        }
        //分页数据列表
        if($term['pageSize'] > 0){
            $item = daohangSelect([
                'cache'   => true,
                'status'  => 'normal',
                'term_id' => $term['term_id'],
                'limit'   => $term['pageSize'],
                'page'    => $term['pageNumber'],
                'sort'    => $term['sortName'],
                'order'   => $term['sortOrder'],
            ]);
            //变量赋值
            if($item){
                $this->assign($item);
            }
        }
        //变量赋值
        $this->assign($term);
        //加载模板
        return $this->fetchTpl($this->site['action'],$term['term_tpl']);
    }
}