<?php
namespace app\admin\controller;

use app\admin\controller\Admin;

//TERM表公共方法

class Term extends Admin
{
    //定义表单初始数据
    protected function formData()
    {
        if( $id = input('id/d',0) ){
            return model('common/Term','loglic')->getId($id, false);
		}
        return [];
    }
    
    //删除(数据库)
	public function delete()
    {
		$ids = input('id/a');
		if(!$ids){
			$this->error(lang('errorIds'));
		}
        
        model('common/Term','loglic')->deleteIds($ids);

        $this->success(lang('success'));
	}
    
    //快速修改状态
    public function status()
    {
        if( !$ids = input('post.id/a') ){
            $this->error(lang('errorIds'));
        }
        
        $result = model('common/Term','loglic')->status($ids,input('request.value/s', 'hidden'));
        
        $this->success(lang('success'));
    }
    
    //快速修改标识
    public function type()
    {
        if( !$ids = input('post.id/a') ){
            $this->error(lang('errorIds'));
        }
        
        $result = dbUpdate('common/Term',['term_id'=>['in',$ids]], ['term_type'=>input('request.value/s', 'common')]);
        
        DcCacheTag('common/Term/Item', NULL);
        
        $this->success(lang('success'));
    }
    
    //更新统计按IDS
    public function count()
    {
        if( !$ids = input('post.id/a') ){
            $this->error(lang('errorIds'));
        }
        
        $result = model('common/Term','loglic')->count($ids);
        
        $this->success(lang('success'));
    }
    
    //预览
    public function preview()
    {
        if(!$info= model('common/Term','loglic')->getId(input('id/d',0), false)){
            $this->error(lang('empty'));
        }
        //去掉后台入口文件
        $url = str_replace($this->request->baseFile(), '', model('common/Term','loglic')->url($info));
        //跳转至前台
        $this->redirect($url, 302);
    }
}