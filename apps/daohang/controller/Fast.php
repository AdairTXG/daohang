<?php
namespace app\daohang\controller;

use app\daohang\controller\Front;

class Fast extends Front
{
    private $isVip = false;
    
    protected $auth = [
         'check'       => true,
         'none_login'  => ['daohang/fast/index'],
         'none_right'  => '*',
         'error_login' => 'user/center/login',
         'error_right' => '',
    ];
    
    public function _initialize()
    {
        $this->request->filter('trim,strip_tags,htmlspecialchars');
        
        $this->requestCheck();
        
        parent::_initialize();
    }
    
    //快速收录
    public function index()
    {
        $this->assign([
            'seoTitle'       => daohangSeo(config('daohang.fast_title')),
            'seoKeywords'    => daohangSeo(config('daohang.fast_keywords')),
            'seoDescription' => daohangSeo(config('daohang.fast_description')),
            'scoreFast'      => intval(config('daohang.score_fast')),//扣除积分
            'scoreRecharge'  => intval(config('user.score_recharge')),//充值比例
            'fields'         => DcFormItems(model('daohang/Fast','loglic')->fields('index')),//表单字段
        ]);
        return $this->fetch();
    }
    
    //空操作
    public function _empty($action='')
    {
        $this->actionCheck($action);
        
        $this->assign([
            'scoreFast'      => intval(config('daohang.score_fast')),//扣除积分
            'scoreRecharge'  => intval(config('user.score_recharge')),//充值比例
            'fields'         => DcFormItems(model('daohang/Fast','loglic')->fields($action)),
        ]);
        
        return $this->fetchTpl($action,'empty');
    }
    
    //保存表单
    public function save()
    {
        //用户组与积分处理
        $this->authFast(config('daohang.score_fast'));
        
        //获取表单数据
        $data = model('daohang/Info','loglic')->postSet($this->site['user']['user_id'], input('post.'));
        //必填字段
        if(!$data['category_id']){
            $this->error( lang('daohang/require/category_id') );
        }
        if(!$data['info_content']){
            $this->error( lang('daohang/require/info_content') );
        }
        //默认数据
        $data['info_type']        = 'fast';
        $data['info_status']      = 'normal';
        
        //保存至数据库
        if( !DcArrayResult(daohangSave($data,true)) ){
		    $this->error(\daicuo\Info::getError());
        }
        
        //普通用户扣除积分
        if($this->isVip == false && config('daohang.score_fast')){
            //积分扣除
            $result = userScoreDec($this->site['user']['user_id'], config('daohang.score_fast'));
            //积分日志
            if($result){
                model('daohang/Log','loglic')->scoreFast($this->site['user']['user_id'],config('daohang.score_fast')*-1,'index',$this->request->ip());
            }
        }
        
        //返回结果
        $this->success(lang('success'));
    }
    
    //手动验证快审发布权限与积分处理权限
    private function authFast($scoreConfig=0)
    {
        //验证规则
        $this->auth['rule'] = 'daohang/fast/save';
        //VIP用户组
        if ( \daicuo\Auth::check($this->auth['rule'], $this->site['user']['user_capabilities'], $this->site['user']['user_caps']) ) {
            $this->isVip = true;
            return true;
        }
        //积分扣除配置（小于1不启用积分发布功能，只可以VIP用户组发布）
        if( $scoreConfig < 1 ){
            $this->error( DcError(lang('daohang/error/fast_vip')), 'daohang/publish/index');
        }
        //积分不足
        if($this->site['user']['user_score'] < $scoreConfig){
            $this->error( DcError(lang('daohang/error/score_light')), 'user/center/index');
        }
    }
}