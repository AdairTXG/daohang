<div class="container pt-3">
  <p class="text-md-center mb-2">
    Copyright © 2019-2022 {:config('common.site_domain')} All rights reserved
    {if config('common.site_icp')}<a class="text-dark" href="https://beian.miit.gov.cn" target="_blank">{:config('common.site_icp')}</a>{/if}
  </p>
  <p class="text-md-center">
    {volist name=":DcTermNavbar(['controll'=>'navs','type'=>'link','status'=>['eq','normal'],'sort'=>'term_order','order'=>'desc'])" id="link"}
    <a class="text-muted" href="{$link.navs_link}" target="{$link.navs_target}">{$link.navs_name|DcSubstr=0,6,false}</a>
    {/volist}
  </p>
</div>