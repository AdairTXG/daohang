<?php
namespace app\user\loglic;

class Datas
{
    //删除基础配置
    public function remove()
    {
        model('common/Config','loglic')->unInstall('user');
        model('common/Field','loglic')->unInstall('user');
        model('common/Route','loglic')->unInstall('user');
        model('common/Role','loglic')->unInstall('user');
        model('common/Auth','loglic')->unInstall('user');
        model('common/Navs','loglic')->unInstall('user');
        model('common/Menu','loglic')->unInstall('user');
        return true;
    }
    
    //删除数据内容
    public function delete()
    {
        //删除VIP权限节点
        db('op')->where(['op_controll'=>'auth','op_name'=>['eq','vip']])->delete();
        //删除插件配置表
        \daicuo\Op::delete_module('user');
        //删除插件队列表
        \daicuo\Term::delete_module('user');
        //删除插件用户表
        \daicuo\User::delete_module('user');
        //返回结果
        return true;
    }
}