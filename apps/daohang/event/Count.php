<?php
namespace app\daohang\event;

use app\common\controller\Addon;

class Count extends Addon
{
	public function _initialize()
    {
		parent::_initialize();
	}
    
	public function index()
    {
        echo number_format(model('daohang/Count','loglic')->views());
	}
}