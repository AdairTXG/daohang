<?php
namespace app\daohang\loglic;

class Log
{
    private $error = '';
    
    public function getError(){
        return $this->error;
    }
    
    //免审发布日志
    public function scoreFast($userId=0, $logValue=0, $logAction='index', $logIp='127.0.0.1')
    {
        $data = [];
        $data['log_name']     = 'daohang/log/fast/'.$logAction;
        $data['log_user_id']  = $userId;
        $data['log_info_id']  = 0;
        $data['log_value']    = $logValue;
        $data['log_module']   = 'daohang';
        $data['log_controll'] = 'fast';
        $data['log_action']   = $logAction;
        $data['log_type']     = 'userScore';
        $data['log_ip']       = $logIp;
        //$data['log_query']  = http_build_query(input('get.'));
        //$data['log_info']   = $this->request->header('user-agent');
        return model('common/Log','loglic')->save($data);
    }
}