<?php
namespace app\daohang\loglic;

class Term 
{
    public function categorySelect($action='index'){
        return DcTermCheck([
            'field'    => 'term_id,term_parent,term_name',
            'module'   => ['eq','daohang'],
            'controll' => ['eq','category'],
            'action'   => ['eq',$action],
        ]);
    }
    
    public function categoryOption($action='index'){
        $item = daohangCategorySelect([
            'result'   => 'array',
            'module'   => 'daohang',
            'controll' => 'category',
            'action'   => 'index',
            'status'   => 'normal',
            'with'     => false,
            'action'   => $action,
            'field'    => 'term_id,term_name',
        ]);
        return array_column($item,'term_name','term_id');
    }
}