<?php
define('INSTALL_NAME', 'daohang');

define('THINK_PATH', __DIR__ .'/thinkphp/');

define('EXTEND_PATH', __DIR__ .'/extend/');

define('VENDOR_PATH', __DIR__ .'/vendor/');

define('APP_PATH', __DIR__ . '/apps/');

define('RUNTIME_PATH', __DIR__ .'/datas/');

define('BIND_MODULE', 'admin/mysql');

require THINK_PATH.'base.php';

\think\App::route(false);

\think\App::run()->send();