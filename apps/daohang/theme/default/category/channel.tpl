{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords}" />
<meta name="description" content="{$seoDescription}" />
{/block}
<!-- -->
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
{include file="widget/ads970" /}
<div class="row dh-row">
  {volist name=":daohangCategorySelect([
    'result' => 'array',
    'action' => 'index',
    'status' => 'normal',
    'parent' => $term_id,
    'field'  => 'term_id,term_name,term_slug,term_slug',
    'with'   => false,
    'sort'   => 'term_order',
    'order'  => 'desc',
  ])" id="category"}
  {if $items=daohangSelect([
    'term_id' => $category['term_id'],
    'field'   => 'info_id,info_id,info_name,info_slug,info_type,info_action,info_update_time',
    'with'    => 'info_meta,term',
    'action'  => 'index',
    'status'  => 'normal',
    'limit'   => 30,
    'sort'    => 'info_id',
    'order'   => 'desc'
    ])}
  <div class="col-12 px-1">
    <div class="card mb-2">
      <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-navicon mr-1 text-purple"></i>{$category.term_name}</span>
        <a class="text-purple small" href="{:daohangUrlCategory($category)}" target="_self">全部>></a>
      </div>
      <div class="card-body row pb-0 mb-0">
        {volist name="$items" id="web"}
        <ul class="col-4 col-md-2 col-lg-auto w-10 list-unstyled text-truncate text-center">
          <li class="w-100"><a class="{$web.info_color|daohangColor}" href="{:daohangUrlInfo($web)}" title="{$web.info_name|DcHtml}">{$web.info_name|daohangSubstr=0,6,false}</a></li>
        </ul>
        {/volist}
      </div>
    </div>
  </div>
  {/if}
  {/volist}
  <!---->
</div>
</div>
<!---->
{/block}
<!-- -->
{block name="footer"}{include file="widget/footer" /}{/block}