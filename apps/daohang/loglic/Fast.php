<?php
namespace app\daohang\loglic;

class Fast 
{
    public function fields($action='index')
    {
        $items  = [
            'info_action' => [
                'order'  => 0,
                'type'   => 'hidden',
                'value'  => $action,
            ],
            'info_name' => [
                'type'      => 'text',
                'maxlength' => '250',
                'required'  => true,
            ],
            'info_referer' => [
                'type'      => 'referer',
                'maxlength' => '250',
                'required'  => true,
            ],
            'image_ico' => [
                'type'        => 'image',
            ],
            'image_level' => [
                'type'        => 'image',
            ],
            'category_id'     => [
                'type'        => 'select',
                'multiple'    => false,
                'size'        => 1,
                'option'      => model('daohang/Term','loglic')->categorySelect($action),
                'placeholder' => false,
            ],
            'tag_name' => [
                'type'        => 'tags',
                'option'      => daohangTags(config('daohang.limit_tag_input'), $action),
                'placeholder' => false,
                'class_tags_list' => 'mr-2',
            ],
            'info_content' => [
                'type'        => 'textarea',
                'rows'        => 3,
            ],
        ];
        
        //动态扩展字段（可精确到操作名）
        if($customs = daohangMetaList('fast', $action)){
            $items = DcArrayPush($items, DcFields($customs, $data), 'info_content');
        }
        
        //公共属性
        foreach($items as $key=>$value){
            if(!isset($value['title'])){
                $items[$key]['title'] = lang('daohang/title/'.$key);
            }
            if(!isset($value['placeholder'])){
                $items[$key]['placeholder'] = lang('daohang/placeholder/'.$key);
            }
            if(!isset($value['class_left'])){
                $items[$key]['class_left'] = 'col-12 col-md-2';
            }
            if(!isset($value['class_right'])){
                $items[$key]['class_right'] = 'col-12 col-md-10';
            }
        }

        return $items;
    }
}