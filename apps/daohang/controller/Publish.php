<?php
namespace app\daohang\controller;

use app\daohang\controller\Front;

class Publish extends Front
{
    //继承上级
    public function _initialize()
    {
        $this->request->filter('trim,strip_tags,htmlspecialchars');
        
        $this->requestCheck();
        
        parent::_initialize();
    }
    
    //免费收录界面
    public function index()
    {
        $this->assign([
            'seoTitle'       => daohangSeo(config('daohang.publish_title')),
            'seoKeywords'    => daohangSeo(config('daohang.publish_keywords')),
            'seoDescription' => daohangSeo(config('daohang.publish_description')),
            'fields'         => DcFormItems(model('daohang/Publish','loglic')->fields('index')),
        ]);
        return $this->fetch();
    }
    
    //空操作
    public function _empty($action='')
    {
        $this->actionCheck($action);
        
        $this->assign([
            'fields' => DcFormItems(model('daohang/Publish','loglic')->fields($action)),
        ]);
        
        return $this->fetchTpl($action,'empty');
    }
    
    //保存表单
    public function save()
    {
        //发布开关
        if(config('daohang.publish_save') != 'on'){
            $this->error(lang('daohang/error/publish_off'));
        }
        //获取表单数据
        $data = model('daohang/Info','loglic')->postSet($this->site['user']['user_id'], input('post.'));
        //必填字段
        if(!$data['category_id']){
            $this->error( lang('daohang/require/category_id') );
        }
        if(!$data['info_content']){
            $this->error( lang('daohang/require/info_content') );
        }
        //保存至数据库
        if( !DcArrayResult(daohangSave($data,true)) ){
		    $this->error(\daicuo\Info::getError());
        }
        $this->success(lang('success'));
    }
}