<?php
namespace app\daohang\validate;

use think\Validate;

class Info extends Validate
{
	
	protected $rule = [
        'info_name'     => 'require',
        'info_slug'     => 'require',
        'info_module'   => 'require',
        'info_id'       => 'require',
        'info_referer'  => 'require|url|unique_referer',
	];
	
	protected $message = [
        'info_id.require'      => '{%daohang/require/info_id}',
        'info_module.require'  => '{%daohang/require/info_module}',
		'info_name.require'    => '{%daohang/require/info_name}',
        'info_referer.require' => '{%daohang/require/info_referer}',
        'info_referer.url'     => '{%daohang/url/info_referer}',
	];
	
    //验证场景
	protected $scene = [
		'save'    =>  ['info_module','info_name','info_referer'],
		'update'  =>  ['info_module','info_name','info_referer','info_id'],
	];
    
    //网址是否收录
    protected function unique_referer($value, $rule, $data, $field)
    {
        //特殊网址
        if(in_array($value,['javascript:;','#'])){
            return true;
        }
        //查询数据库
        $where = array();
        $where['info_meta_key']   = ['eq','info_referer'];
        $where['info_meta_value'] = ['eq',$value];
        if($data['info_id']){
            $where['info_id'] = ['neq',$data['info_id']];
        }
        $info = db('infoMeta')->where($where)->value('info_meta_id');
        //无记录直接验证通过
        if(is_null($info)){
            return true;
        }
        //已有记录
		return lang('daohang/unique/info_referer');
	}
}