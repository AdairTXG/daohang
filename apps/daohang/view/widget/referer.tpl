<div class="{$form.class|default='row form-group'}">
  <label class="{$form.class_left|default='col-md-2'}" for="{$form.name|DcHtml}">
    <strong>{$form.title|DcHtml}</strong>
  </label>
  <div class="{$form.class_right|default='col-md-6'}">
    <div class="{$form.class_group|default='input-group input-group-sm'}">
      <input {if $form['readonly']}readonly{/if} {if $form['disabled']} disabled{/if} {if $form['required']} required{/if} {if $form['autofocus']} autofocus{/if} type="text" class="{$form.class_right_control|default='form-control form-control-sm'}" id="{:DcEmpty($form['id'],$form['name'])}" name="{$form.name|DcHtml}" value="{$form.value|DcHtml}" placeholder="{$form.placeholder|DcHtml}" maxlength="{$form.maxlength|DcHtml}" autocomplete="{$form.autocomplete|DcSwitch}">
      <div class="input-group-append">
        <button class="btn btn-secondary" type="button" data-toggle="referer" data-target="#{:DcEmpty($form['id'],$form['name'])}" data-url="{:DcUrlAdmin('daohang/referer/index',['url'=>''])}">{:lang('daohang/btn/parse')}</button>
      </div>
    </div>
  </div>
  {if $form['tips']}
  <div class="{$form.class_tips|default='col-md-2 form-text text-muted small'}">
    {$form.tips}
  </div>
  {/if}
</div>
<script>
$(document).on("click", '[data-toggle="referer"]', function() {
    var url = jQuery($(this).data('target')).val();
    if(!url){
        daicuo.bootstrap.dialog("{:lang('daohang/error/referer_empty')}");
        return false;
    }
    var $this = $(this);
    $(this).text('loading...');
    daicuo.ajax.get($(this).data('url')+url, function($result, $xhr, $status){
        if($result.code){
            jQuery.each($result.data,function(index, value){
                if(value){
                    $('#'+index).val(value);
                }
            });
            $this.parent().remove();
        }else{
            $this.text("{:lang('daohang/btn/parse')}");
            daicuo.bootstrap.dialog($result.msg);
        }
    },function($xhr, $status){
        daicuo.bootstrap.dialog($xhr);
    });
});
</script>