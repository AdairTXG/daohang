<?php
namespace app\daohang\behavior;

use think\Controller;

class Hook extends Controller
{
    //后台首页统计
    public function adminIndexCount(&$datas)
    {
        array_push($datas,[
            'id'    => 'daohangView',
            'title' => 'admin/daohang/count/view',
            'ico'   => 'fa-adjust',
            'color' => 'text-info',
            'count' => 1,
        ]);
        array_push($datas,[
            'id'    => 'detail',
            'title' => 'admin/daohang/count/detail',
            'ico'   => 'fa-file-text',
            'color' => 'text-primary',
            'count' => model('daohang/Count','loglic')->detail(),
        ]);
        array_push($datas,[
            'id'    => 'detail',
            'title' => 'admin/daohang/count/hidden',
            'ico'   => 'fa-file-text-o',
            'color' => 'text-info',
            'count' => model('daohang/Count','loglic')->hidden(),
        ]);
        array_push($datas,[
            'id'    => 'category',
            'title' => 'admin/daohang/count/category',
            'ico'   => 'fa-folder-o',
            'color' => 'text-danger',
            'count' => model('daohang/Count','loglic')->category(),
        ]);
        array_push($datas,[
            'id'    => 'tag',
            'title' => 'admin/daohang/count/tag',
            'ico'   => 'fa-tags',
            'color' => 'text-warning',
            'count' => model('daohang/Count','loglic')->tag(),
        ]);
    }
    
    //后台首页页脚
    public function adminIndexFooter(&$params)
    {
        echo('<script>$(document).ready(function(){$("#daohangView").load("'.DcUrlAddon(["module"=>"daohang","controll"=>"count","action"=>"index"]).'")});</script>');
    }
    
    //前台权限扩展
    public function adminCapsFront(&$caps)
    {
        $caps = array_merge($caps,[
            'daohang/score/save',
            'daohang/fast/save',
            'daohang/filter/index',
            'daohang/search/common',
            'daohang/referer/index',
            'daohang/data/index',
        ]);
    }
    
    //后台权限扩展
    public function adminCapsBack(&$caps)
    {
        $caps = array_merge($caps,[
            'daohang/admin/index',
            'daohang/admin/save',
            'daohang/admin/delete',
            'daohang/collect/index',
            'daohang/collect/save',
            'daohang/collect/delete',
            'daohang/collect/write',
        ]);
    }
}