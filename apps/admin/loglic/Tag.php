<?php
namespace app\admin\loglic;

class Tag
{
    public function fields($data=[])
    {
        $module   = DcEmpty($data['term_module'],'index');
        $controll = 'tag';
        $action   = DcEmpty($data['term_action'],'index');
        $fields = [
            'html_1' => [
                'order'           => 1,
                'type'            => 'html',
                'value'           => '<div class="row"><div class="col-12 col-md-8">',
            ],
            'term_id' => [
                'order'           => 2,
                'type'            => 'hidden',
                'value'           => $data['term_id'],
                'data-filter'     => false,
                'data-visible'    => true,
                'data-width'      => '80',
                'data-width-unit' => 'px',
                'data-sortable'   => true,
                'data-sort-name'  => 'term_id',
                'data-order'      => 'asc',
            ],
            'term_name' => [
                'order'           => 4,
                'type'            => 'text',
                'value'           => $data['term_name'],
                'required'        => true,
                'data-filter'     => false,
                'data-visible'    => true,
                'data-align'      => 'left',
            ],
            'term_slug' => [
                'order'           => 5,
                'type'            => 'text',
                'value'           => $data['term_slug'],
                'data-filter'     => false,
                'data-visible'    => true,
                'data-align'      => 'left',
            ],
            'term_title' => [
                'order'           => 6,
                'type'            => 'text',
                'value'           => $data['term_title'],
            ],
            'term_keywords' => [
                'order'           => 7,
                'type'            => 'text',
                'value'           => $data['term_keywords'],
            ],
            'term_description' => [
                'order'           => 8,
                'type'            => 'text',
                'value'           => $data['term_description'],
            ],
            'term_info' => [
                'order'           => 9,
                'type'            => 'text',
                'value'           => $data['term_info'],
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'term_limit' => [
                'order'           => 0,
                'type'            => 'number',
                'value'           => $data['term_limit'],
            ],
            'term_tpl' => [
                'order'           => 0,
                'type'            => 'text',
                'value'           => $data['term_tpl'],
            ],
            'html_2'      => [
                'order'           => 0,
                'type'            => 'html',
                'value'           => '</div><div class="col-12 col-md-4">',
                'order'           => 199,
            ],
            'term_status' => [
                'order'           => 0,
                'type'            => 'select',
                'value'           => DcEmpty($data['term_status'],'normal'),
                'option'          => model('common/Attr','loglic')->statusOption(),
                'data-filter'     => true,
                'data-visible'    => false,
            ],
            'term_status_text' => [
                'order'           => 92,
                'data-title'      => lang('term_status'),
                'data-visible'    => true,
                'data-width'      => '100',
            ],
            'term_type' => [
                'order'           => 93,
                'type'            => 'select',
                'option'          => model('common/Attr','loglic')->tagType(),
                'value'           => $data['term_type'],
                'data-filter'     => true,
                'data-visible'    => true,
                'data-width'      => 100,
            ],
            'term_parent' => [
                'order'           => 91,
                'type'            => 'text',
                'value'           => intval($data['term_parent']),
                'data-filter'     => false,
                'data-visible'    => true,
                'data-width'      => 100,
            ],
            'term_order' => [
                'order'           => 95,
                'type'            => 'text',
                'value'           => intval($data['term_order']),
                'data-filter'     => false,
                'data-visible'    => true,
                'data-sortable'   => true,
                'data-width'      => '100',
            ],
            'term_count' => [
                'order'           => 96,
                'data-filter'     => false,
                'data-visible'    => true,
                'data-sortable'   => true,
                'data-width'      => '100',
            ],
            'term_action' => [
                'order'           => 97,
                'type'            => 'text',
                'value'           => $action,
                'data-filter'     => true,
                'data-visible'    => true,
                'data-width'      => 100,
                'data-value'      => '',
            ],
            'term_controll' => [
                'order'           => 98,
                'type'            => 'text',
                'value'           => $controll,
                'disabled'        => true,
                'data-filter'     => false,
                'data-visible'    => true,
                'data-width'      => 100,
            ],
            'term_module' => [
                'order'           => 99,
                'type'            => 'text',
                'value'           => $module,
                'data-filter'     => true,
                'data-visible'    => true,
                'data-width'      => 100,
                'data-value'      => '',
            ],
            'html_3' => [
                'order'           => 16,
                'type'            => 'html',
                'value'           => '</div></div>',
            ]
        ];
        //动态扩展字段
        $customs = model('common/Term','loglic')->metaList($module, $controll, $action);
        //合并所有字段
        if($customs){
            $fields = DcArrayPush($fields, DcFields($customs, $data), 'html_2');
        }
        //返回所有表单字段
        return $fields;
    }
}