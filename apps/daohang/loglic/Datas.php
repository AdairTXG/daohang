<?php
namespace app\daohang\loglic;

class Datas
{
    //删除基础配置
    public function remove()
    {
        //应用配置表（路由、配置、权限、字段、采集规则）
        \daicuo\Op::delete_module('daohang');
        //后台菜单
        model('common/Menu','loglic')->unInstall('daohang');
        //前台导航
        model('common/Navs','loglic')->unInstall('daohang');
        //返回结果
        return true;
    }
    
    //删除数据内容
    public function delete()
    {
        //应用配置表（路由、配置、权限、字段）
        \daicuo\Op::delete_module('daohang');
        
        //删除插件分类/标签/导航
        \daicuo\Term::delete_module('daohang');
        
        //删除内容数据
        \daicuo\Info::delete_module('daohang');
        
        //返回结果
        return true;
    }
}