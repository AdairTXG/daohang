<?php
namespace app\daohang\controller;

use app\daohang\controller\Front;

class Detail extends Front
{

    public function _initialize()
    {
        $this->requestCheck();
        
        parent::_initialize();
    }
    
    //首页
    public function index()
    {
        return $this->detail();
    }
    
    //空操作
    public function _empty($action='')
    {
        //模型验证
        $this->actionCheck($action);
        //查询数据
        return $this->detail();
    }
    
    //公共操作
    private function detail()
    {
        if( isset($this->query['id']) ){
            $info = daohangId($this->query['id']);
        }elseif( isset($this->query['slug']) ){
            $info = daohangSlug($this->query['slug']);
        }elseif( isset($this->query['name']) ){
            $info = daohangName($this->query['name']);
        }else{
            $this->error(lang('mustIn'),'daohang/index/index');
        }
        //数据判断
        if(!$info){
            $this->error(lang('daohang/error/empty'),'daohang/index/index');
        }
        //SEO标签
        $info['seoTitle'] = daohangSeo(DcEmpty($info['info_title'],$info['info_name']));
        $info['seoKeywords'] = daohangSeo(DcEmpty($info['info_keywords'],$info['info_name']));
        $info['seoDescription'] = daohangSeo(DcEmpty($info['info_description'],$info['info_excerpt']));
        //主分类ID
        $info['term_id'] = intval($info['category_id'][0]);
        //分享链接
        $info['info_share_url'] = $this->request->root(true).daohangUrlInfo($info);
        //增加人气值
        daohangInfoInc($info['info_id'], 'info_views');
        //变量赋值
        $this->assign($info);
        //加载模板
        return $this->fetchTpl($this->site['action'],$info['info_tpl']);
    }
}