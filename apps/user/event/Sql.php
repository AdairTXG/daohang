<?php
namespace app\user\event;

class Sql
{
    /**
    * 安装时触发
    * @return bool 只有返回true时才会往下执行
    */
	public function install()
    {
        //初始配置
        model('user/Install','loglic')->config();
        
        //扩展字段
        model('user/Install','loglic')->field();
        
        //初始路由
        model('user/Install','loglic')->route();

        //初始角色
        model('user/Install','loglic')->role();
        
        //初始权限
        model('user/Install','loglic')->auth();
        
        //后台菜单
        model('user/Install','loglic')->menu();
        
        //前台菜单（导航栏、侧边栏等）
        model('user/Install','loglic')->navs();
        
        //初始用户
        model('user/Install','loglic')->user();
        
        //清空缓存
        \think\Cache::clear();

        //返回结果
        return true;
	}
    
    /**
    * 升级时触发
    * @return bool 只有返回true时才会往下执行
    */
    public function upgrade()
    {
        //更新状态
        model('user/Upgrade','loglic')->status();
        
        //清空缓存
        \think\Cache::clear();
        
        return true;
    }
    
    /**
    * 卸载时触发
    * @return bool 只有返回true时才会往下执行
    */
    public function remove()
    {
        return model('user/Datas','loglic')->remove();
    }
    
    /**
    * 删除时触发
    * @return bool 只有返回true时才会往下执行
    */
    public function unInstall()
    {
        return model('user/Datas','loglic')->delete();
	}
}