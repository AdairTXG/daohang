<?php
namespace app\daohang\loglic;

use app\common\loglic\Update;

class Upgrade extends Update
{
    //更新应用状态
    public function status()
    {
        return \daicuo\Apply::updateStatus('daohang', 'enable');
    }
    
    //更新应用打包
    public function pack()
    {
        if(config('common.apply_module') != 'daohang'){
            return true;
        }
        return \daicuo\Op::write([
            'apply_name'    => '呆错网址导航系统',
            'apply_module'  => 'daohang',
            'apply_version' => config('apply.version'),
            'apply_rely'    => 'user',
        ], 'common', 'config', 'system', 0, 'yes');
    }
    
    //更新应用配置
    public function config()
    {
        return true;
    }
    
    //更新后台菜单
    public function menu()
    {
        return true;
    }
    
    //更新前台菜单
    public function navs()
    {
        return true;
    }
    
    //更新初始分类
    public function category()
    {
        return true;
    }
    
    //更新初始标签
    public function tag()
    {
        return true;
    }
}