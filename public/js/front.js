// 轮播滑动
window.daicuo.carousel = {
    // 默认配置
    defaults : {
        selector: '[data-toggle="carousel"]',
        initialIndex: 0,
        freeScroll: true,
        cellAlign: "left",
        resize: true,
        contain: true,
        lazyLoad: true,
        prevNextButtons: false,
        pageDots: false
    },
    // 插件初始化
    init: function(options){
        //合并默认配置
        options = $.extend({}, this.defaults, options || {});
        //动态加载插件库
        if( $(options.selector).length > 0 ){
            daicuo.carousel.ajaxLoad(function(){
                window.daicuo.carousel.plusInit(options);
            });
        }
    },
    // 插件方法：重置
    resize: function(options) { //resize
        //合并动态配置选择器
        options = $.extend({selector:this.defaults.selector}, options || {});
        //执行插件方法
        daicuo.carousel.ajaxLoad(function(){
            $(options.selector).flickity('resize');
        });
    },
    // 动态加载插件库并回调
    ajaxLoad: function(callback) {
        daicuo.ajax.script(['https://lib.baomitu.com/flickity/2.2.0/flickity.pkgd.min.js'],function(){
            //加载CSS
            daicuo.ajax.css('https://lib.baomitu.com/flickity/2.2.0/flickity.min.css');
            //执行回调
            daicuo.tools.callBack(callback);
        });
    },
    // 插件库初始调用方法
    plusInit: function(options){
        $(options.selector).each(function(i) {
            var $index = $(this).find('.active').index() * 1;
            if ($index > 3) {
                options.initialIndex = $index - 3;
            }
            $(this).flickity(options);
        });
    },
    // 1.5之前版本
    nav: function(selector) {
        selector = selector || '[data-toggle="carousel"]'; //if ($selector === undefined)
        daicuo.carousel.init({selector:selector});
    }
};

// 简繁转换组件
window.daicuo.language = {
    // 记录最后操作
    type: '',
    // 默认值
    defaults : {
        selector: '',//document.body
        method: 'auto'//s2t||t2s||auto||refresh
    },
    // 初始化调用
    init: function(options){
        //合并配置
        options = $.extend({}, this.defaults, options);
        //调用不同方法
        if(options.method  == 's2t'){
            daicuo.language.s2t(options.selector);
        }else if(options.method  == 't2s'){
            daicuo.language.t2s(options.selector);
        }else if(options.method  == 'auto'){
            daicuo.language.auto(options.selector);
        }else if(options.method  == 'refresh'){
            daicuo.language.refresh(options.selector);
        }
    },
    // 简转繁
    s2t: function(selector) {
        selector = selector || document.body;//undefined
        daicuo.language.ajaxLoad(function() {
            daicuo.language.type = 's2t';
            $(selector).s2t();
        });
    },
    // 繁转简
    t2s: function(selector) {
        selector = selector || document.body;
        daicuo.language.ajaxLoad(function() {
            daicuo.language.type = 't2s';
            $(selector).t2s();
        });
    },
    // 自动转为浏览器相关
    auto: function(selector) {
        if (daicuo.browser.language == 'zh-hk' || daicuo.browser.language == 'zh-tw') {
            daicuo.language.s2t(selector);
        }else if(daicuo.browser.language == 'zh-cn'){
            daicuo.language.t2s(selector);
        }
    },
    // Dom更新后需重新激活
    refresh: function(selector) {
        if (daicuo.language.type == 's2t') {
            daicuo.language.s2t(selector);
        } else {
            daicuo.language.t2s(selector);
        }
    },
    // 动态加载插件库并回调
    ajaxLoad: function(callback) {
        daicuo.ajax.script(["https://cdn.daicuo.cc/jquery.s2t/0.1.0/s2t.min.js"], function(){
            daicuo.tools.callBack(callback);
        });
    },
    // 1.5前旧方法刷新
    dom: function(selector) {
        daicuo.language.refresh(selector);
    }
};

// 图片延迟加载组件
window.daicuo.lazyload = {
    // 默认配置
    defaults: {
        selector: 'img[data-original]',
        placeholder: daicuo.root + "public/images/grey.gif",
        effect: "fadeIn",
        failurelimit: 10
        //threshold : 400
        //skip_invisible : false
        //container: $(".carousel-inner"),
    },
    // 初始化
    init: function(options){
        //合并配置
        options = $.extend({}, this.defaults, options);
        //动态加载插件并执行初始化方法
        if( $(options.selector).length>0 ){
            daicuo.lazyload.ajaxLoad(function() {
                window.daicuo.lazyload.plusInit(options);
            });
        }
    },
    // Dom更新后需重新激活
    refresh: function(selector) {
        //目标元素
        selector = selector || this.defaults.selector;
        //回调插件
        daicuo.lazyload.ajaxLoad(function(){
            $(selector).lazyload();//'#selector img[data-original]'
        });
    },
    // 动态加载插件包
    ajaxLoad: function(callback) {
        daicuo.ajax.script(["https://lib.baomitu.com/jquery.lazyload/1.9.1/jquery.lazyload.min.js"], function(){
            daicuo.tools.callBack(callback);
        });
    },
    // 插件库的初始化调用方法
    plusInit: function(options){
        $(options.selector).lazyload(options);
    },
    // 1.5 before 旧方法
    image: function(selector) {
        selector = selector || this.defaults.selector;
        daicuo.lazyload.init({selector:selector});
    },
    // 1.5 before 旧方法
    dom: function(selector) {
        selector = selector || this.defaults.selector;
        daicuo.lazyload.refresh(selector);
    }
};

// AJAX翻页
window.daicuo.page = {
    // 分页锁
    locked: false,
    // 动态配置
    options: {},
    // 默认配置
    defaults: {
        selectorClick: '[data-toggle="page"],[data-pageClick="true"]',
        selectorScroll: '[data-toggle="page"],[data-pageScroll="true"]'
    },
    // 初始化调用
    init: function(options){
        //合并初始参数
        daicuo.page.options = $.extend({}, this.defaults, options);
        //监听点击事件
        daicuo.page.click();
        //监听滚动事件
        daicuo.page.scroll();
    },
    // 点击翻页
    click: function() {
        //选择器
        var selector = daicuo.page.options.selectorClick || daicuo.page.defaults.selectorClick;
        //点击事件
        $(document).on("click", selector, function() {
            if (daicuo.page.locked == false) {
                daicuo.page.ajax($(this), $(this).attr('data-url'), $(this).attr('data-page') * 1 + 1, $(this).attr('data-target'));
            }
        });
    },
    // 滚动翻页
    scroll: function() {
        //选择器
        var selector = daicuo.page.options.selectorScroll || daicuo.page.defaults.selectorScroll;
        //只需一个
        var $obj = $(selector).eq(0);
        if ($obj.length) {
            $(window).bind("scroll", function() {
                if (daicuo.page.locked == false && ($(this).scrollTop() + $(window).height()) >= $(document).height()) {
                    daicuo.page.ajax($obj, $obj.attr('data-url'), $obj.attr('data-page') * 1 + 1, $obj.attr('data-target'));
                }
            });
        }
    },
    // 请求数据
    ajax: function($event, $url, $page, $target) {
        var $html = $event.html();
        $(document).ajaxStart(function() {
            daicuo.page.locked = true;
            $event.html(daicuo.lang.loading);
        });
        $.get($url + $page,function($data) {
            daicuo.page.locked = false;
            $event.html($html);
            if ($data) {
                //追加数据
                $($target).append($data);
                //更新页码
                $event.attr("data-page", $page);
                //window.history.pushState(null, null, $url+$page);
                //图片懒加载
                if( $event.data('target-lazyload') ){
                    daicuo.lazyload.init({selector:$event.data('target-lazyload')});
                }
                //是否简繁转化
                if( $event.data('target-language') ){
                    daicuo.language.refresh($event.data('target-language'));
                }
                //自定义回调事件
                daicuo.tools.callBack($event.data('call-back'));
            } else {
                $event.remove();
                $event.unbind("click");
            }
        });
    }
};