<?php
namespace app\daohang\controller;

use app\daohang\controller\Front;

class Tag extends Front
{
    //继承上级
    public function _initialize()
    {
        $this->requestCheck();
        
        parent::_initialize();
    }
    
    //首页
    public function index()
    {
        return $this->detail();
    }
    
    //空操作
    public function _empty($action='')
    {
        //模型验证
        $this->actionCheck($action);
        //查询数据
        return $this->detail();
    }
    
    //公共操作
    private function detail()
    {
        if( isset($this->query['id']) ){
            $term = daohangTagId($this->query['id']);
        }elseif( isset($this->query['slug']) ){
            $term = daohangTagSlug($this->query['slug']);
        }elseif( isset($this->query['name']) ){
            $term = daohangTagName($this->query['name']);
        }else{
            $this->error(lang('mustIn'),'daohang/index/index');
        }
        //数据为空
        if(!$term){
            $this->error(lang('empty'),'daohang/index/index');
        }
        //SEO标签
        $term['seoTitle']       = daohangSeo(DcEmpty($term['term_title'],$term['term_name']),$this->site['page']);
        $term['seoKeywords']    = daohangSeo(DcEmpty($term['term_keywords'],$term['term_name']),$this->site['page']);
        $term['seoDescription'] = daohangSeo(DcEmpty($term['term_description'],$term['term_name']),$this->site['page']);
        //分页路径
        $term['pagePath']   = daohangUrlTag($term,'[PAGE]');
        //地址栏
        $term['pageSize']   = intval(config('daohang.limit_tag'));
        $term['pageNumber'] = $this->site['page'];
        $term['sortName']   = 'info_order desc,info_update_time';
        $term['sortOrder']  = 'desc';
        //分页数量
        if(is_numeric($term['term_limit'])){
            $term['pageSize'] = $term['term_limit'];
        }
        //分页数据
        if($term['pageSize'] > 0){
            $item = daohangSelect([
                'cache'   => true,
                'status'  =>'normal',
                'action'  => $this->site['action'],
                'term_id' => $term['term_id'],
                'limit'   => $term['pageSize'],
                'page'    => $term['pageNumber'],
                'sort'    => $term['sortName'],
                'order'   => $term['sortOrder'],
            ]);
            //变量赋值
            if($item){
                $this->assign($item);
            }
        }
        //变量赋值
        $this->assign($term);
        //加载模板
        return $this->fetchTpl($this->site['action'],$term['term_tpl']);
    }
}