<div class="container pt-2">
<p class="text-md-center mb-2">
  Copyright © 2020-2022 {:config('common.site_domain')} All rights reserved
  {if config('common.site_icp')}
  <a href="https://beian.miit.gov.cn" target="_blank">{:config('common.site_icp')}</a>
  {/if}
  {if config('common.site_gongan')}
  <img src="{$path_root}public/images/gongan.png" alt="gongan" width="16" height="16">
  <a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode={:daohangGongan()}" target="_blank">{:config('common.site_gongan')}</a>
  {/if}
</p>
<p class="text-md-center">
  {volist name=":DcTermNavbar(['status'=>['in',['normal','public']],'type'=>'link','sort'=>'term_order','order'=>'desc'])" id="links"}
  <a class="text-dark" href="{$links.navs_link}" target="{$links.navs_target}">{$links.navs_name|daohangSubstr=0,6,false}</a>
  {/volist}
</p>
</div>